<?php

use Faker\Generator as Faker;

$factory->define(App\Page::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'slug' => $faker->company,
        'content' => $faker->text,
    ];
});
