<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', ['as'=>'home', 'uses'=>'GeneralController@index']);

Route::get('/login', ['as' => 'login',
    function () {
        return redirect()->route('home');
    }
]);

Route::post('/login',    ['as'=>'login', 'uses'=>'GeneralController@postLogin']);
Route::post('/register', ['as'=>'register', 'uses'=>'GeneralController@postRegister']);

Route::get('/register', ['as' => 'register',
    function () {
        return redirect()->route('home');
    }
]);


Route::group(['middleware' => ['auth', 'myApplication']], function() {
    Route::post('applications/postupdate/{id}', ['as'=>'applications.postupdate', 'uses'=>'ApplicationController@postupdate']);
    Route::post('applications/approve/{id}',    ['as'=>'applications.approve', 'uses'=>'ApplicationController@approve']);
    Route::resource('applications', 'ApplicationController');
});

Route::group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => ['auth', 'isAdmin']], function() {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@index']);
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController', ['except' => 'show']);
    Route::resource('permissions', 'PermissionController', ['except' => 'show']);
    Route::get('applications-admin/show2/{id}',    ['as'=>'applications-admin.show2',   'uses'=>'ApplicationController@show2']);
    Route::post('applications-admin/approve/{id}', ['as'=>'applications-admin.approve','uses'=>'ApplicationController@approve']);
    Route::resource('applications-admin', 'ApplicationController');
    Route::resource('pages', 'PageController');
    Route::match(['get', 'post'],'/options',    ['as'=>'options', 'uses'=>'AdminController@options']);
});

Route::get('page/{id}',     ['as'=>'page',  'uses'=>'GeneralController@page']);
