jQuery(document).on('change','#mar_left',function(){
	var m_left = jQuery(this).find('input[type="number"]').val();
 	jQuery('#map .map .marker').css('margin-left', m_left+'px');
 	console.log('left '+m_left)
});
jQuery(document).on('change','#mar_top',function(){
	var m_top = jQuery(this).find('input[type="number"]').val();
 	jQuery('#map .map .marker').css('margin-top', m_top+'px');
 	console.log('top '+m_top)
});
jQuery(document).ready(function(){
 	jQuery('#mar_top').trigger('change');
 	jQuery('#mar_left').trigger('change');
});
