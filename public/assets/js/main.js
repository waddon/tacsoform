!function(factory){"use strict";"function"==typeof define&&define.amd?define(["jquery"],factory):jQuery&&!jQuery.fn.hoverIntent&&factory(jQuery)}(function($){"use strict";var cX,cY,_cfg={interval:100,sensitivity:6,timeout:0},INSTANCE_COUNT=0,track=function(ev){cX=ev.pageX,cY=ev.pageY},compare=function(ev,$el,s,cfg){if(Math.sqrt((s.pX-cX)*(s.pX-cX)+(s.pY-cY)*(s.pY-cY))<cfg.sensitivity)return $el.off(s.event,track),delete s.timeoutId,s.isActive=!0,ev.pageX=cX,ev.pageY=cY,delete s.pX,delete s.pY,cfg.over.apply($el[0],[ev]);s.pX=cX,s.pY=cY,s.timeoutId=setTimeout(function(){compare(ev,$el,s,cfg)},cfg.interval)},delay=function(ev,$el,s,out){return delete $el.data("hoverIntent")[s.id],out.apply($el[0],[ev])};$.fn.hoverIntent=function(handlerIn,handlerOut,selector){var instanceId=INSTANCE_COUNT++,cfg=$.extend({},_cfg);$.isPlainObject(handlerIn)?(cfg=$.extend(cfg,handlerIn),$.isFunction(cfg.out)||(cfg.out=cfg.over)):cfg=$.isFunction(handlerOut)?$.extend(cfg,{over:handlerIn,out:handlerOut,selector:selector}):$.extend(cfg,{over:handlerIn,out:handlerIn,selector:handlerOut});var handleHover=function(e){var ev=$.extend({},e),$el=$(this),hoverIntentData=$el.data("hoverIntent");hoverIntentData||$el.data("hoverIntent",hoverIntentData={});var state=hoverIntentData[instanceId];state||(hoverIntentData[instanceId]=state={id:instanceId}),state.timeoutId&&(state.timeoutId=clearTimeout(state.timeoutId));var mousemove=state.event="mousemove.hoverIntent.hoverIntent"+instanceId;if("mouseenter"===e.type){if(state.isActive)return;state.pX=ev.pageX,state.pY=ev.pageY,$el.off(mousemove,track).on(mousemove,track),state.timeoutId=setTimeout(function(){compare(ev,$el,state,cfg)},cfg.interval)}else{if(!state.isActive)return;$el.off(mousemove,track),state.timeoutId=setTimeout(function(){delay(ev,$el,state,cfg.out)},cfg.timeout)}};return this.on({"mouseenter.hoverIntent":handleHover,"mouseleave.hoverIntent":handleHover},cfg.selector)}});

$(function() {
	function ql_scroll() {
		if($(window).width()>1279) {
			var body_top = $(window).scrollTop();
			if(body_top < 1000) {
				$('#quick_links').css('top','-' + body_top + 'px');
			}
		}
	}
	ql_scroll();
	$(window).scroll(function(){
		ql_scroll();
	});
	hiConfig = {
		sensitivity: 3,
		interval: 200,
		timeout: 200,
		over: function() {
			$(this).addClass('hovered');
		},
		out: function() {
			$(this).removeClass('hovered');
		}
	}
	$('header .pc_menu > li').hoverIntent(hiConfig);
	$('header .mob_menu > li').hoverIntent(hiConfig);
	$('.map_wrap .map .marker').hoverIntent(hiConfig);

	$('.latest .item .desc .share .open').click( function(event){
		event.preventDefault();
		$(this).toggleClass('active');
		$(this).siblings('ul').toggleClass('opened');
	});

	$('#pageup').click( function(){
		$("html, body").stop().animate({scrollTop:0}, 500);
	});

	$('#open_mob_ql').click( function(event){
		event.preventDefault();
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			$('#quick_links').show();
		} else {
			$('#quick_links').hide();
		}
	});

	$('#open_mob_menu').click( function(event){
		event.preventDefault();
		$(this).toggleClass('active');
		if($(this).hasClass('active')){
			$('#mob_menu').addClass('opened');
		} else {
			$('#mob_menu').removeClass('opened');
		}
	});
	$('#mob_menu .close_menu').click( function(event){
		$('#open_mob_menu').removeClass('active');
		$('#mob_menu').removeClass('opened');
	});

	$('#open_search').click( function(){
		$('header .searchform').css('width', '50px');
		$('header .searchform').addClass('opened');
		$('header .searchform').stop().animate({width:'50%'}, 500);
	});
	$('header .searchform .close').click( function(){
		$('header .searchform').stop().animate({width:'50px'}, 300);
		setTimeout(function(){$('header .searchform').removeClass('opened')},500)
	});

	$('#tabs li a').click( function(event){
		event.preventDefault();
		var tab = $(this).attr('href');
		$('#tabs li a').removeClass('active');
		$(this).addClass('active');
		$('.tabs_content .tab').removeClass('opened');
		$(tab).addClass('opened');
	});

	$('.calendar_wrap .calendar .col .cal_event').click( function(){
		$('.calendar_wrap .calendar .col .cal_event').removeClass('active');
		$(this).addClass('active');
	});

	$('.cal_event .info .close').click( function(e){
		e.stopPropagation();
		$('.calendar_wrap .calendar .col .cal_event').removeClass('active');
	});

	$('.search_row .btn_more').click( function(e){
		e.stopPropagation();
		$(this).hide();
		$(this).parent('.search_row').find('.hidden').show();
	});

	$('.calendar_wrap .head .change_col_row button').click( function(){
		$('.calendar_wrap .head .change_col_row button').removeClass('active');
		$(this).addClass('active');
		var type = $(this).attr('data-type');
		$('.calendar').attr('data-type',type);
	});

	$('input[type="checkbox"], input[type="radio"], select, input[type="file"]').styler({
	  locale: 'en',
	  locales: {
	    'en': {
	      filePlaceholder: 'No file selected',
	      fileBrowse: 'Browse...',
	      fileNumber: 'Selected files: %s',
	      selectPlaceholder: 'Select...',
	      selectSearchNotFound: 'No matches found',
	      selectSearchPlaceholder: 'Search...'
	    }
	  },
	});

	function one_height(c) {
		var mh = 0;
	  $(c).each(function () {
	    var hb = parseInt($(this).height());
	    if(hb > mh) {mh = hb;};
	  });
	  $(c).height(mh);
	}
	one_height('.latest.latest_news .col:not(.big_item) .item');
	one_height('.latest.latest_photos .col .item');
	one_height('.latest.latest_videos .col .item');
	one_height('.calendar_wrap .calendar .col .cal_event');

	function init_scroll(c) {
			if($(window).width() < 1280) {
				$(".calendar").mCustomScrollbar('destroy');
				$(".calendar").mCustomScrollbar({
				  axis:"x",
				  theme:"dark-3",
				  advanced:{ autoExpandHorizontalScroll:true }
				});
				$('#quick_links').css('min-height', '0');
			} else {
				$(".calendar").mCustomScrollbar('destroy');
				$('#quick_links').css('min-height', '2000px');
			}
	}
	init_scroll();
	$( window ).resize(function() {
		init_scroll();
	});
	$(document).on('click','#gallery_modal .modal_close, #gallery_modal .bg',function(){
		$('#gallery_modal').hide();
		setTimeout(function (){
			$('#gallery_modal').remove();
		}, 500);
	});
	$(document).on('click','.open_gallery',function(e){
		e.preventDefault();
		var link = $(this).attr('href');
		$('footer').before('<div id="gallery_modal"><div class="bg"></div><div class="container"><div class="modal_content" style="min-height:400px;"><span class="modal_close"></span><div id="frame"></div></div></div></div>');
		$('#gallery_modal #frame').load(link);
		setTimeout(function (){
			$('#gallery_modal').show();
		}, 100);
	});
});
