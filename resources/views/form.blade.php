@extends('layouts.layout')

@section('content')
    <h1 class="text-center">Ask for P2P On-demand support</h1>
    {{--<p class="text-justify">EU TACSO 3 project provides flexible and targeted support to address the on-going needs of CSOs, networks, NRCs, EUDs etc. in the Western Balkans and Turkey in the framework of its People-to-People (P2P) Programme. The P2P Programme aims to enable organization of regional or sub-regional events in order to ensure follow-ups at national level and disseminate information and experiences to as many CSOs as possible. The types of activities are listed in the request form below. The application process consists of two steps on-line process: Preliminary application screening (Step 1), Full application submission and decision-making (Step 2).</p>
    <p class="text-justify">Each request will be reviewed. You may be contacted for more details. Each applicant will be notified by email, first on preliminary screening results and after submission full application of the decision whether to support or the reasons for rejection. For any additional information, please address your questions to: <a href="mailto:welcome@tacso.eu">welcome@tacso.eu</a>.</p>
    <p class="text-justify">To ask for the P2P On-demand support, please fill in the online request below. Be aware that all fields are mandatory.</p>--}}
    <div class="card">
        <div class="card-header tab-label-wrap">
            <a class="tab-label active" data-id="tab-login">SING IN</a>
            <a class="tab-label" data-id="tab-register">NEW ACCOUNT</a>
        </div>
        <div class="card-body">
            <div id="tab-login" class="tab-content mt-5">
                <form id="login-form">
                    <div style="padding: 50px;">
                        <div class="form-group">
                            <label for="email" class="">{{ __('E-Mail') }}</label>
                            <input id="email" type="email" class="form-control" name="email" autocomplete="email" autofocus>
                            <label class="message validation-invalid-label"></label>
                        </div>
                        <div class="form-group">
                            <label for="password" class="">{{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control" name="password" autocomplete="current-password">
                            <label class="message validation-invalid-label"></label>
                        </div>
                        <div class="d-flex justify-content-between">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                                @if (Route::has('password.request'))
                                    <a class="" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                        </div>
                    </div>                    
                    <button type="submin" class="btn-action">SING IN</button>
                </form>
            </div>

            <div id="tab-register" class="tab-content mt-5"  style="display:none">
                <form id="register-form">
                    <div style="padding: 50px;">
                        <div class="form-group">
                            <label for="reg-name" class="">{{ __('Name') }}</label>
                            <input id="reg-name" type="text" class="form-control" name="name"  autocomplete="name" autofocus>
                            <label class="message validation-invalid-label"></label>
                        </div>
                        <div class="form-group">
                            <label for="reg-email" class="">{{ __('E-Mail') }}</label>
                            <input id="reg-email" type="email" class="form-control" name="email" value="{{ old('email') }}"  autocomplete="email" autofocus>
                            <label class="message validation-invalid-label"></label>
                        </div>
                        <div class="form-group">
                            <label for="reg-password" class="">{{ __('Password') }}</label>
                            <input id="reg-password" type="password" class="form-control" name="password"  autocomplete="current-password">
                            <label class="message validation-invalid-label"></label>
                        </div>
                        <div class="form-group">
                            <label for="reg-password-confirm" class="">{{ __('Confirm Password') }}</label>
                            <input id="reg-password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password">
                            <label class="message validation-invalid-label"></label>
                        </div>
                    </div>                    
                    <button type="submin" class="btn-action">REGISTER</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $(document).on('click','.tab-label:not(.active)',function(e){

        e.preventDefault();
        $('.tab-label').removeClass('active');
        $(this).addClass('active');
        $('.tab-content').css('display','none');
        $('#'+$(this).attr('data-id')).fadeIn();
    });

    $('#login-form').submit(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var postData = {
            'email': $('input[name=email]').val(),
            'password': $('input[name=password]').val(),
            'remember_me': $('input[name=remember]').is(':checked')
        }
        $.ajax({
            type: 'POST',
            url: '/login',
            data: postData,
            success: function(response){
                window.location.href = response.redirect;
            },
            error: function(response){
                console.log(response);
                $('#tab-login .message').text(response.responseJSON.error);
                errors = response.responseJSON.errors || [];
                $( '#tab-login input:not([type=checkbox])' ).each(function( index ) {
                    let fieldname = $( this ).attr('name');
                    $(this).siblings('.message').first().text('');
                    if (errors[fieldname]){
                        $(this).siblings('.message').first().text( errors[fieldname].toString() );
                    }
                });
                if (response.responseJSON.error) {
                    $('#tab-login .message').text(response.responseJSON.error);
                }
            }
        });
    });

    $('#register-form').submit(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var postData = {
            'name': $('#tab-register input[name="name"]').val(),
            'email': $('#tab-register input[name="email"]').val(),
            'password': $('#tab-register input[name="password"]').val(),
            'password_confirmation': $('#tab-register input[name="password_confirmation"]').val(),
        }
        $.ajax({
            type: 'POST',
            url: '/register',
            data: postData,
            success: function(response){
                window.location.href = response.redirect;
            },
            error: function(response){
                errors = response.responseJSON.errors;
                $( '#tab-register input' ).each(function( index ) {
                    let fieldname = $( this ).attr('name');
                    $(this).siblings('.message').first().text('');
                    if (errors[fieldname]){
                        $(this).siblings('.message').first().text( errors[fieldname].toString() );
                    }
                });
                console.log(response);
            }
        });
    });
});
</script>
@endsection
