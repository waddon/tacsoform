@extends('layouts.layout-admin')

@section('content')

{{ Form::open(array('route' => 'users.index')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

                    <!-- Left content -->
                    <div class="order-2 order-md-1 w-100">
                        <div class="card" id="release">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Parameters</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <div class="form-group {{ $errors->has('name') ? 'has-danger' : '' }}">
                                    {{ Form::label('name', 'Name', ['class'=>'small text-muted font-italic']) }}
                                    {{ Form::text('name', null, array('class' => 'form-control'. ($errors->has('name') ? ' is-invalid' : '' ))) }}

                                    @if($errors->has('name'))
                                        @foreach ($errors->get('name') as $message)
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group {{ $errors->has('email') ? 'has-danger' : '' }}">
                                    {{ Form::label('email', 'E-mail', ['class'=>'small text-muted font-italic']) }}
                                    {{ Form::email('email', null, array('class' => 'form-control'. ($errors->has('email') ? ' is-invalid' : '' ))) }}

                                    @if($errors->has('email'))
                                        @foreach ($errors->get('email') as $message)
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    {{ Form::label('password', 'Password', ['class'=>'small text-muted font-italic']) }}<br>
                                    {{ Form::password('password', array('class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}

                                    @if($errors->has('password'))
                                        @foreach ($errors->get('password') as $message)
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group">
                                    {{ Form::label('password_confirmation', 'Confirm Password', ['class'=>'small text-muted font-italic']) }}<br>
                                    {{ Form::password('password_confirmation', array('class' => 'form-control'. ($errors->has('password') ? ' is-invalid' : '' ))) }}
                                    @if($errors->has('password_confirmation'))
                                        @foreach ($errors->get('password_confirmation') as $message)
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @endforeach
                                    @endif
                                </div>

                                <div class='form-group'>
                                    {{ Form::label('roles', 'Roles', ['class'=>'small text-muted font-italic']) }}<br>
                                    @foreach ($roles as $role)
                                        <div class="custom-control custom-checkbox">
                                            {{ Form::checkbox('roles[]',  $role->id , null, ['id' => $role->name, 'class' => 'custom-control-input'. ($errors->has('roles') ? ' is-invalid' : '' )]) }}
                                            {{ Form::label($role->name, ucfirst($role->name), ['class' => 'custom-control-label']) }}
                                        </div>
                                    @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- /left content -->

                    <!-- Right sidebar component -->
                    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
                        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
                            <div class="sidebar-content">
                                <div class="card">
                                    <div class="card-header bg-transparent header-elements-inline">
                                        <span class="text-uppercase font-size-sm font-weight-semibold">Navigation</span>
                                        <div class="header-elements">
                                            <div class="list-icons">
                                                <a class="list-icons-item" data-action="collapse"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                            <div class="btn-group w-100">
                                                <button type="submit" id="submit" class="btn btn-success w-50" title="Save"><i class="icon-floppy-disk"></i></button>
                                                <a href="{{ route('users.index') }}" class="btn btn-danger w-50" title="Exit"><i class="icon-esc"></i></a>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /right sidebar component -->

    </div>
{{ Form::close() }}


@endsection

