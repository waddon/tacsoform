@extends('layouts.layout-admin')

@section('content')
    <div class="row">
        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-blue-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{$stat['users'] ?? ''}}</h3>
                        <span class="text-uppercase font-size-xs">total users</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-users icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-warning-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{$stat['wait'] ?? ''}}</h3>
                        <span class="text-uppercase font-size-xs">wait for approve</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-bell3 icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-xl-3">
            <div class="card card-body bg-success-400 has-bg-image">
                <div class="media">
                    <div class="media-body">
                        <h3 class="mb-0">{{$stat['complete'] ?? ''}}</h3>
                        <span class="text-uppercase font-size-xs">application complete</span>
                    </div>
                    <div class="ml-3 align-self-center">
                        <i class="icon-checkmark icon-3x opacity-75"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
