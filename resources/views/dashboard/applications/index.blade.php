@extends('layouts.layout-admin')

@section('content')
    <div class="card mb-3">

        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-approve" class="nav-link active" data-toggle="tab"><i class="icon-bell3 mr-2"></i> Wait for Approve</a></li>
                <li class="nav-item"><a href="#tab-complete" class="nav-link" data-toggle="tab"><i class="icon-checkmark mr-2"></i> Complete</a></li>
                <li class="nav-item"><a href="#tab-underway" class="nav-link" data-toggle="tab"><i class="icon-pencil mr-2"></i> Underway</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-approve">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableApprove">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-complete">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableComplete">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-underway">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableUnderway">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script>
    var pageLength = 10;
    var columns = [
            {"data":"id"},
            {"data":"name","searchable":false,"orderable":false},
            {"data":"status"},
            {"data":"created"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 2, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
        ];


    var tableApprove = $("#datatableApprove").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('applications-admin.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"approve"},
        },
        "columns":columns,
        'columnDefs': columnDefs,
    });
    var tableComplete = $("#datatableComplete").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('applications-admin.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"complete"},
        },
        "columns":columns,
        'columnDefs': columnDefs,
    });
    var tableUnderway = $("#datatableUnderway").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('applications-admin.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"underway"},
        },
        "columns":columns,
        'columnDefs': columnDefs,
    });

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-destroy')
@endsection
