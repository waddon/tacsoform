@extends('layouts.layout-admin')

@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <form method="POST">
                @csrf
                <div class="row">
                    <!-- Pages before Annex 1 -->
                        <div class="col-lg-6 mb-3">
                            <h3>Pages before Annex 1</h3>
                            <input type="hidden" name="infopagesAnnex1" value="{{isset($options['infopagesAnnex1']) ? $options['infopagesAnnex1'] : '[]'}}">
                            <div class="pages">
                                @if(isset($infopagesAnnex1))
                                    @foreach($infopagesAnnex1 as $key => $infopageAnnex1)
                                        <div class="page d-flex align-items-center">
                                            <div class="page-element page-move p-2"><i class="icon-grid"></i></div>
                                            <div class="page-element page-name p-2 flex-grow-1 flex-shrink-1" data-id="{{$infopageAnnex1->id}}">{{$infopageAnnex1->name}}</div>
                                            <div class="page-element page-remove p-2"><i class="icon-trash"></i></div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <button type="button" class="btn btn-success page-add" data-toggle="modal" data-target="#modal-page-add"> + </button>
                        </div>
                    <!-- /Pages before Annex 1 -->

                    <!-- Pages before Annex 2 -->
                        <div class="col-lg-6 mb-3">
                            <h3>Pages before Annex 2</h3>
                            <input type="hidden" name="infopagesAnnex2" value="{{isset($options['infopagesAnnex2']) ? $options['infopagesAnnex2'] : '[]'}}">
                            <div class="pages">
                                @if(isset($infopagesAnnex2))
                                    @foreach($infopagesAnnex2 as $key => $infopageAnnex2)
                                        <div class="page d-flex align-items-center">
                                            <div class="page-element page-move p-2"><i class="icon-grid"></i></div>
                                            <div class="page-element page-name p-2 flex-grow-1 flex-shrink-1" data-id="{{$infopageAnnex2->id}}">{{$infopageAnnex2->name}}</div>
                                            <div class="page-element page-remove p-2"><i class="icon-trash"></i></div>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            <button type="button" class="btn btn-success page-add" data-toggle="modal" data-target="#modal-page-add"> + </button>
                        </div>
                    <!-- Pages before Annex 2 -->

                    <!-- Top Menu -->
                        <div class="col-lg-6 mb-3">
                            <h3>Top Menu</h3>
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Page About')</label>
                                <div class="col-sm-8">
                                    <select name="pageAbout" id="pageAbout" class="form-control">
                                        <option value="0">@lang('Select')</option>
                                        @if(isset($pages))
                                            @foreach($pages as $key => $page)
                                                @if($page->public)
                                                    <option value="{{$page->id}}" {{ (isset($options['pageAbout']) && $options['pageAbout']==$page->id) ? 'selected' : ''}}>{{$page->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Page FAQ')</label>
                                <div class="col-sm-8">
                                    <select name="pageFAQ" id="pageFAQ" class="form-control">
                                        <option value="0">@lang('Select')</option>
                                        @if(isset($pages))
                                            @foreach($pages as $key => $page)
                                                @if($page->public)
                                                    <option value="{{$page->id}}" {{ (isset($options['pageFAQ']) && $options['pageFAQ']==$page->id) ? 'selected' : ''}}>{{$page->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Page Contacts')</label>
                                <div class="col-sm-8">
                                    <select name="pageContacts" id="pageContacts" class="form-control">
                                        <option value="0">@lang('Select')</option>
                                        @if(isset($pages))
                                            @foreach($pages as $key => $page)
                                                @if($page->public)
                                                    <option value="{{$page->id}}" {{ (isset($options['pageContacts']) && $options['pageContacts']==$page->id) ? 'selected' : ''}}>{{$page->name}}</option>
                                                @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    <!-- /Top Menu -->

                    <!-- Download File -->
                        <div class="col-lg-6 mb-3">
                            <h3>Downloading</h3>
                            <div class="form-group row">
                                <label for="" class="col-form-label col-sm-4">@lang('Download File')</label>
                                <div class="col-sm-8">
                                    <input type="text" name="downloadFile" id="downloadFile" class="form-control" value="{{ isset($options['downloadFile']) ? $options['downloadFile'] : ''}}">
                                </div>
                            </div>
                        </div>
                    <!-- /Download File -->

                </div>
                <button class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

@section('style')
<style>
    .page-move{
        cursor: move;
    }
    .page-remove{
        cursor: pointer;
    }
</style>
@endsection

@section('scripts')
<script src="/assets-admin/js/plugins/extensions/jquery_ui/interactions.min.js"></script>
<script>
    var sender;

    $(document).on('click','.page-remove',function() {
        pagelist = $(this).closest('.pages');
        $(this).closest('.page').remove();
        pagelist.siblings('input[type=hidden]').val(JSON.stringify( create_array(pagelist) ));
    })

    $(document).on('click','.page-remove',function() {
        pagelist = $(this).closest('.pages');
        $(this).closest('.page').remove();
        pagelist.siblings('input[type=hidden]').val(JSON.stringify( create_array(pagelist) ));
    })

    $(document).on('click','.page-add',function(){
        sender = $(this).siblings('.pages');
        // pagelist = $(this).siblings('.pages');
        // pagelist.append( renderTemplate('page-empty',{}) );
        // pagelist.siblings('input[type=hidden]').val(JSON.stringify( create_array(pagelist) ));
    });

    $('.pages').sortable({
        handle: ".page-move",
        tolerance: 'pointer',
        opacity: 0.6,
        placeholder: 'sortable-placeholder',
        start: function(e, ui){
            ui.placeholder.height(ui.item.outerHeight());
        },
        stop : function(e,ui){
            pagelist = $(this);
            pagelist.siblings('input[type=hidden]').val(JSON.stringify( create_array(pagelist) ));
        }
    });

    function create_array(pagelist) {
        var array = [];
        pagelist.find('.page-name').each(function(){
            console.log($(this).attr('data-id'));
            array.push($(this).attr('data-id'));
        });
        return array;
    }

    function renderTemplate(name, data) {
        var template = document.getElementById(name).innerHTML;
        for (var property in data) {
            if (data.hasOwnProperty(property)) {
                var search = new RegExp('{' + property + '}', 'g');
                template = template.replace(search, data[property]);
            }
        }
        return template;
    }

    $('#modal-page-add-submit').click(function(){
        if( $('#page-select').val() != 0 ){
            sender.append( renderTemplate('page-empty',{
                pageId: $('#page-select').val(),
                pageName: $("#page-select option:selected").text(),
            }) );
            sender.siblings('input[type=hidden]').val(JSON.stringify( create_array(sender) ));
        }
    });

</script>
@endsection

@section('modals')
<template id="page-empty">
    <div class="page d-flex align-items-center">
        <div class="page-element page-move p-2"><i class="icon-grid"></i></div>
        <div class="page-element page-name p-2 flex-grow-1 flex-shrink-1" data-id="{pageId}">{pageName}</div>
        <div class="page-element page-remove p-2"><i class="icon-trash"></i></div>
    </div>
</template>

<div id="modal-page-add" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title">Append page</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <select id="page-select" class="form-control" data-sender="">
                    <option value="0"> - Select Page - </option>
                    @if(isset($pages))
                        @foreach($pages as $key => $page)
                            <option value="{{$page->id}}">{{$page->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="modal-page-add-submit">Append</button>
            </div>
        </div>
    </div>
</div>
@endsection
