@extends('layouts.layout-admin')

@section('content')

{{ Form::model($model, array('url' => $model->id ? route('pages.update', $model->id) : route('pages.store'), 'method' => $model->id ? 'PUT' : 'POST')) }}

<div class="d-flex align-items-start flex-column flex-md-row">

    <div class="order-2 order-md-1 w-100">
        <div class="card mb-3">
            <div class="card-header bg-transparent header-elements-inline">
                <h5 class="card-title">@lang('Parameters')</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    {{ Form::label('name', 'Name', ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('name', $model->name, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group row">
                    {{ Form::label('slug', 'Slug', ['class'=>'small text-muted font-italic col-md-4 col-form-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('slug', $model->slug, array('class' => 'field form-control')) }}
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('content', 'Content', ['class'=>'small text-muted font-italic col-form-label']) }}
                    {{ Form::textarea('content', $model->content, ['class' => 'editor-full field form-control', 'id' => 'editor']) }}
                </div>
            </div>
        </div>

    </div>

    <!-- Right sidebar component -->
    <div class="sidebar-sticky w-100 w-md-auto order-1 order-md-2">
        <div class="sidebar sidebar-light sidebar-component sidebar-component-right sidebar-expand-md mb-3">
            <div class="sidebar-content">
                <div class="card">
                    <div class="card-header bg-transparent header-elements-inline">
                        <span class="text-uppercase font-size-sm font-weight-semibold">Navigation</span>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <p>
                            <span class="small text-muted font-italic">@lang('Created')</span>
                            <span class="float-right">{{$model->created_at}}</span>
                        </p>
                        <p>
                            <span class="small text-muted font-italic">@lang('Updated')</span>
                            <span class="float-right">{{$model->updated_at}}</span>
                        </p>
                        <p class="form-check form-check-right form-check-switchery">
                            <label class="form-check-label">
                                <span class="small text-muted font-italic">Public</span>
                                <input type="checkbox" name="public" class="form-check-input-switchery"@if($model->public) checked @endif data-fouc>
                            </label>
                        </p>
                    </div>
                    <div class="card-body">
                        <div class="btn-group w-100">
                            <button type="submit" id="submit" class="btn btn-success w-50" title="@lang('Save')"><i class="icon-floppy-disk"></i></button>
                            {{--<a href="{{ route('pages.show', $model->id) }}" class="btn btn-light w-50" title="@lang('Preview')"><i class="icon-eye"></i></a>--}}
                            <a href="{{ route('pages.index') }}" class="btn btn-danger w-50" title="@lang('Exit')"><i class="icon-esc"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /right sidebar component -->

</div>

{{ Form::close() }}

@endsection

@section('scripts')
@include('template-parts.save_ajax_form')
<script src="/assets-admin/js/plugins/notifications/sweet_alert.min.js""></script>
<script src="/assets-admin/js/plugins/editors/ckeditor2019/ckeditor.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    var elems = Array.prototype.slice.call(document.querySelectorAll('.form-check-input-switchery'));
    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
    $('.editor-full').each(function(e){
        CKEDITOR.replace(this.id, {
            height: 400,
            extraPlugins: 'forms'
        });
    });    
    $('form').on('submit', function(e){
        e.preventDefault();
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
        save_ajax_form( $(this), "{!! route('pages.index') !!}");
    });
});    
</script>
@endsection
