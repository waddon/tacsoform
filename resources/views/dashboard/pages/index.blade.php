@extends('layouts.layout-admin')

@section('content')
    <div class="card mb-3">

        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-highlight">
                <li class="nav-item"><a href="#tab-active" class="nav-link active" data-toggle="tab"><i class="icon-menu7 mr-2"></i> Active</a></li>
                <li class="nav-item"><a href="#tab-trash" class="nav-link" data-toggle="tab"><i class="icon-trash mr-2"></i> Trash</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade show active" id="tab-active">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableActive">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Public</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="d-flex mt-3">
                        <a href="{{ route('pages.create') }}" class="btn btn-success" title="Append"><i class="icon-file-plus"></i></a>
                    </div>                    
                </div>
                <div class="tab-pane fade" id="tab-trash">
                    <div class="table-responsive">
                        <table class="table datatable-basic table-striped dataTable no-footer mb-3 w-100" id="datatableTrash">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Public</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
<script>
    var pageLength = 10;
    var columns = [
            {"data":"id"},
            {"data":"name"},
            {"data":"slug"},
            {"data":"public"},
            {"data":"actions","searchable":false,"orderable":false}
        ];
    var columnDefs = [
            { "targets": 0, "className": "text-center", },
            { "targets": 3, "className": "text-center", },
            { "targets": 4, "className": "text-center", },
        ];


    var tableActive = $("#datatableActive").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('pages.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"active"},
        },
        "columns":columns,
        'columnDefs': columnDefs,
    });
    var tableTrash = $("#datatableTrash").DataTable({
        "autoWidth" : true,
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "pageLength": pageLength,
        "bSortCellsTop": true,
        "order": [[ 0, "desc" ]],
        "ajax": {
            "url": "{{route('pages.datatable')}}",
            "dataType": "json",
            "type": "POST",
            "data": {"_token":"{{ csrf_token() }}", "table":"trash"},
        },
        "columns":columns,
        'columnDefs': columnDefs,
    });

</script>
@endsection
@section('modals')
@include('template-parts.modal-ajax-actions')
@endsection
