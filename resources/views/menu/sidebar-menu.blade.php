<ul class="nav nav-sidebar" data-nav-type="accordion">
    <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Main</div> <i class="icon-menu" title="Main"></i></li>
    <li class="nav-item">
        <a href="{{route('dashboard')}}" class="nav-link {!! Auth::user()->uriSegment(2) === '' ? 'active' : '' !!}">
            <i class="icon-home4"></i><span>Dashboard</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('applications-admin.index')}}" class="nav-link {!! Auth::user()->uriSegment(2) === 'applications-admin' ? 'active' : '' !!}">
            <i class="icon-drawer3"></i><span>Applications</span>
        </a>
    </li>
    <li class="nav-item nav-item-submenu">
        <a href="#" class="nav-link"><i class="icon-users"></i> <span>Users</span></a>
        <ul class="nav nav-group-sub" data-submenu-title="Users">
            <li class="nav-item"><a href="{{ route('users.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'users' ? 'active' : '' !!}">User list</a></li>
            <li class="nav-item"><a href="{{ route('roles.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'roles' ? 'active' : '' !!}">Roles</a></li>
            <li class="nav-item"><a href="{{ route('permissions.index') }}" class="nav-link {!! Auth::user()->uriSegment(2) === 'permissions' ? 'active' : '' !!}">Permissions</a></li>
        </ul>
    </li>
    <li class="nav-item">
        <a href="{{route('pages.index')}}" class="nav-link {!! Auth::user()->uriSegment(2) === 'pages' ? 'active' : '' !!}">
            <i class="icon-stack-text"></i><span>Pages</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="{{route('options')}}" class="nav-link {!! Auth::user()->uriSegment(2) === 'options' ? 'active' : '' !!}">
            <i class="icon-cog"></i><span>Options</span>
        </a>
    </li>

</ul>
