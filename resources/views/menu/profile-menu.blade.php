<ul class="nav nav-sidebar">
    <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="icon-user-plus"></i>
            <span>My profile</span>
        </a>
    </li>
    <li class="nav-item">
        <a href="#" class="nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="icon-switch2"></i>
            <span>Logout</span>
        </a>
    </li>
</ul>
