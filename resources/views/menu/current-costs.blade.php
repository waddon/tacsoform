<div class="card">
    <div class="card-header header-elements-inline">
        <h6 class="card-title">Monthly spending</h6>
    </div>
    <div class="table-responsive">
        <table class="table text-nowrap">
            <thead>
                <tr>
                    <th class="w-100">Category</th>
                    <th>Plan</th>
                    <th>Fact</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-home7"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Home</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">2 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-car"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Auto & Transport</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">250.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-indigo-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-store"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Food & Dining</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">2 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">933.05</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-success-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-gift"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Gifts & Donations</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">2 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">3 583.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-bag"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Shopping</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">0.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <!-- -->

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-primary-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-person"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Health & Medical</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">1 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">377.64</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-piggy-bank"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Investment & Vacation</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">12 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-indigo-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-movie"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Entertainment</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">0.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-success-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-graduation"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">School</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">900.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>

                <tr>
                    <td>
                        <div class="d-flex align-items-center">
                            <div class="mr-3">
                                <a href="#" class="btn bg-danger-400 rounded-round btn-icon btn-sm legitRipple">
                                    <i class="icon-question7"></i>
                                </a>
                            </div>
                            <div><a href="#" class="text-default font-weight-semibold letter-icon-title">Other</a></div>
                        </div>
                    </td>
                    <td class="text-right"><h6 class="text-muted mb-0">1 000.00</h6></td>
                    <td class="text-right"><h6 class="font-weight-semibold mb-0">0.00</h6></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>