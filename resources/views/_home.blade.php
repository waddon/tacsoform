@extends('layouts.layout')

@section('content')
        <h2>Annex 1: Preliminary Application</h2>
        <div class="card">
            <h1 class="text-center">Preliminary Application</h1>
            <form id="annex1">

                <div class="form-group">
                    <p>1. Name</p>
                    <input type="text" id="q1" name="q1" placeholder="Enter your name" required>
                </div>

                <div class="form-group">
                    <p>2. Type of Organisation you present</p>
                    <div class="mb-2">
                        <input type="radio" id="q2_1" name="q2" required value="EU Institution or EU Delegation in an IPA Beneficiary">
                        <label for="q2_1"> EU Institution or EU Delegation in an IPA Beneficiary</label>
                    </div>
                    <div class="mb-2">
                        <input type="radio" id="q2_2" name="q2" required value="A Civil Society Organisation (Social partner; Non-Governmental Organisation; CSO Network; Grassroots organisation; Individual from an IPA Beneficiary">
                        <label for="q2_2"> A Civil Society Organisation (Social partner; Non-Governmental Organisation; CSO Network; Grassroots organisation; Individual from an IPA Beneficiary:</label>
                        <div class="pl-4 mt-2" id="q2_2_0">
                            <div class="mb-2">
                                <input type="radio" id="q2_2_1" name="q2_2">
                                <label for="q2_2_1"> Albania</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_2" name="q2_2">
                                <label for="q2_2_2"> Bosnia and Herzegovina</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_3" name="q2_2">
                                <label for="q2_2_3"> Kosovo*</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_4" name="q2_2">
                                <label for="q2_2_4"> Montenegro</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_5" name="q2_2">
                                <label for="q2_2_5"> North Macedonia</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_6" name="q2_2">
                                <label for="q2_2_6"> Serbia</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2_7" name="q2_2">
                                <label for="q2_2_7"> Turkey</label>
                            </div>
                        </div>
                    </div>
                    <div class="mb-2">
                        <input type="radio" id="q2_3" name="q2" required value="A National Resource Centre">
                        <label for="q2_3"> A National Resource Centre</label>
                    </div>
                    <div class="mb-2">
                        <input type="radio" id="q2_4" name="q2" required value="Other (Please specify)">
                        <label for="q2_4"> Other (Please specify)</label>
                    </div>
                    <input type="text" id="q2_4_1" name="q2_4_1" placeholder="Enter organisation's name" class="mt-1">
                </div>

                <div class="form-group">
                    <p>3. Name of the organisation you represent:</p>
                    <input type="text" id="q3" name="q3" placeholder="Enter organisation's name" required>
                </div>

                <div class="form-group">
                    <p>4. Website and social media of your organisation (if applicable):</p>
                    <input type="text" id="q4" name="q4" placeholder="http://tacso.eu" required>
                </div>

                <div class="form-group">
                    <p>5. Position / role in the organisation, if applicable:</p>
                    <input type="text" id="q5" name="q5" placeholder="Position / role" required>
                </div>
                <div class="form-group">
                    <p>6. Email:</p>
                    <input type="email" id="q6" name="q6" placeholder="Enter email" required>
                </div>
                <div class="form-group">
                    <p>7. Phone Number:</p>
                    <input type="text" id="q7" name="q7" placeholder="Enter number" required>
                </div>
                <div class="form-group">
                    <p>8. Topic of proposed event:</p>
                    <input type="text" id="q8" name="q8" placeholder="Event's topic" required>
                </div>

                <div class="form-group">
                    <p>9. Type of event</p>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_1" name="q9[]" required value="Trainings">
                        <label for="q9_1"> Trainings</label>
                    </div>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_2" name="q9[]" required value="Thematic or sectoral events">
                        <label for="q9_2"> Thematic or sectoral events</label>
                    </div>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_3" name="q9[]" required value="Study visit">
                        <label for="q9_3"> Study visit</label>
                    </div>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_4" name="q9[]" required value="Networking">
                        <label for="q9_4"> Networking</label>
                    </div>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_5" name="q9[]" required value="Workshop and forums">
                        <label for="q9_5"> Workshop and forums</label>
                    </div>
                    <div class="mb-2">
                        <input type="checkbox" id="q9_6" name="q9[]" required value="Coaching and mentoring">
                        <label for="q9_6"> Coaching and mentoring</label>
                    </div>
                </div>


                <div class="text-center">
                    <button type="submit" class="btn btn_green btn-send">SEND</button>
                </div>

            </form>
        </div>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        showhide();
        $('#annex1').submit(function(e){
            e.preventDefault();
        });

        $('input').change(function(){
            showhide();
        });

        function showhide(){

            if($('#q2_2:checked').length==1){
                $('#q2_2_0').show();
                $('[name=q2_2]').prop('required',true);
            } else {
                $('#q2_2_0').hide();
                $('[name=q2_2]').removeAttr('required');
            }

            if($('#q2_4:checked').length==1){
                $('#q2_4_1').show();
                $('#q2_4_1').prop('required',true);
            } else {
                $('#q2_4_1').hide();
                $('#q2_4_1').removeAttr('required');
            }

            if($('input[name="q9[]"]:checked').length==0){
                $('input[name="q9[]"]').prop('required',true);
            } else {
                $('input[name="q9[]"]').removeAttr('required');
            }
        };
    });
</script>
@endsection
