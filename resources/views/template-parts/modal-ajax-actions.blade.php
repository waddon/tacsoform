<div id="modal-ajax-action" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Delete element</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                Удаление в корзину
            </div>
            <form id="form-modal-ajax" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
            <input name="_method" type="hidden" value="POST"></form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn" data-dismiss="modal" id="modal-ajax-submit">Delete</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.btn-ajax-action',function(){
            $('#modal-ajax-action .modal-title').text( $(this).attr('title') );
            $('#modal-ajax-action .modal-body').text( $(this).attr('data-text') );
            $('#modal-ajax-action .modal-header').attr( 'class', 'modal-header ' + $(this).attr('data-color') );
            $('#modal-ajax-action #modal-ajax-submit').attr( 'class', 'btn ' + $(this).attr('data-color') );
            $('#modal-ajax-action #modal-ajax-submit').html( $(this).attr('title') );
            $('#modal-ajax-action input[name=_method]').val( $(this).attr('data-method') );
            $('#form-modal-ajax').attr( 'action',$(this).attr('href') );
        });

        $('#modal-ajax-submit').click(function(){
            $.ajax({
                url: $('#form-modal-ajax').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#form-modal-ajax').serialize(),
                success: function (data) {
                    tableActive.ajax.reload(null, false);
                    tableTrash.ajax.reload(null, false);
                },
                error: function(){
                    alert('Ajax error');
                }
            });
        });
    })
</script>
