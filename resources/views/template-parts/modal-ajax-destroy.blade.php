<div id="modal-ajax-destroy" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title">Delete element</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="remove-modal-body">
                Are you sure you want to permanently delete the item? Recovery of the item will be impossible.
            </div>
            <form id="form-modal-ajax-destroy" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf
            <input name="_method" type="hidden" value="DELETE"></form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn bg-danger" data-dismiss="modal" id="modal-ajax-destroy-submit">Delete</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-view" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h2 class="modal-title">Annex 1</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal-view-body">
                <table class="table table-striped">
                    <thead><tr><th>Parameter</th><th>Value</th></tr></thead>
                    <tbody id="f1">
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-approve" class="modal fade" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title">Approve</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal-approve-body">
                Are you sure you want to approve this application?.
            </div>
            <form id="form-modal-approve" action="#" method="POST" style="display: none;">@csrf
            {{--<input name="_method" type="hidden" value="DELETE">--}}</form>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn bg-success" data-dismiss="modal" id="modal-approve-submit">Approve</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-view2" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h2 class="modal-title">Full Application Form</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="modal-view2-body">
                <ul class="nav nav-tabs nav-tabs-highlight">
                    <li class="nav-item"><a href="#tab-f2" class="nav-link active" data-toggle="tab">General Information</a></li>
                    <li class="nav-item"><a href="#tab-f3" class="nav-link" data-toggle="tab">Event/Activity Information</a></li>
                    <li class="nav-item"><a href="#tab-f4" class="nav-link" data-toggle="tab">Event/Activity Budget</a></li>
                    <li class="nav-item"><a href="#tab-agenda" class="nav-link" data-toggle="tab">Agenda</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-f2">
                        <h2 class="text-center">General Information on the Applicant and Event/Activity</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead><tr><th>Parameter</th><th>Value</th></tr></thead>
                                <tbody id="f2">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-f3">
                        <h2 class="text-center">Event/Activity Information</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead><tr><th>Parameter</th><th>Value</th></tr></thead>
                                <tbody id="f3">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-f4">
                        <h2 class="text-center">Event/Activity Budget / Requested Support</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead><tr><th>Parameter</th><th>Value</th></tr></thead>
                                <tbody id="f4">
                                </tbody>
                            </table>
                            <div class="mt-3" id="uploaded-file"></div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tab-agenda">
                        <h2 class="text-center">AGENDA</h2>
                        <div id="agenda-wrapper" class="modal-field"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.btn-ajax-destroy',function(){
            $('#form-modal-ajax-destroy').attr('action',$(this).attr('href'));
        });
        $(document).on('click','.btn-view, .btn-view2',function(){
            get_modal_content( $(this).attr('href') );
        });

        $('#modal-ajax-destroy-submit').click(function(){
            $.ajax({
                url: $('#form-modal-ajax-destroy').attr('action'),
                type: 'POST',
                dataType : "json",
                data: $('#form-modal-ajax-destroy').serialize(),
                success: function (data) {
                    tableActive.ajax.reload(null, false);
                    //tableTrash.ajax.reload(null, false);
                },
                error: function(){
                    alert('Ajax error');
                }
            });
        });
        function get_modal_content(route){
            $('.modal-field').empty();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: route,
                success: function(response){
                    $( "#f1" ).html(response.f1);
                    $( "#f2" ).html(response.f2);
                    $( "#f3" ).html(response.f3);
                    $( "#f4" ).html(response.f4);
                    var temp_link = '';
                    if (response.file && response.file.url && response.file.name){
                        temp_link = 'Uploaded file: <a href="/storage/'+response.file.url+'" download>'+response.file.name+'</a>';
                    }
                    $( '#uploaded-file' ).html(temp_link);
                    if (response.agenda){
                        $('#agenda-wrapper').empty();
                        $.each( response.agenda, function( i, val ) {
                            $('#agenda-wrapper').append('<h3 class="text-center">' + val.title + '</h3>');
                            var temp = '<div class="table-responsive mb-3"><table class="table table-striped"><thead><tr><th>Time</th><th>Session Title/Brief Description</th><th>Speaker/Presenter/Moderator</th><th></th></tr></thead>'+val.part1+val.break+val.part2+val.lunch+val.part3+'</table></div>';
                            $('#agenda-wrapper').append(temp);
                        });
                        $('#agenda-wrapper').find('*').removeAttr('contenteditable');
                        $('#agenda-wrapper').find('button').remove();
                    }
                },
                error: function(response){
                    console.log(response);
                }
            });
            return true;
        }

        $(document).on('click','.btn-approve',function(){
            $('#form-modal-approve').attr('action',$(this).attr('href'));
        });
        $('#modal-approve-submit').click(function(){
            $.ajax({
                type: 'POST',
                url: $('#form-modal-approve').attr('action'),
                data: $('#form-modal-approve').serialize(),
                success: function(response){
                    tableApprove.ajax.reload(null, false);
                    tableComplete.ajax.reload(null, false);
                    tableUnderway.ajax.reload(null, false);
                },
                error: function(response){
                    console.log(response);
                }
            });
        });

    })
</script>
