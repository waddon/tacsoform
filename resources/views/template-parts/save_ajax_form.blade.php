{{--<script src="{{ asset('/limitless_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>--}}
<script type="text/javascript">
        function save_ajax_form(form, url){
                let data = {};
                $.ajax({
                    url: form.attr('action'),
                    type: form.attr('method'),
                    data: form.serialize(),
                }).done((data, status, xhr) => {
                    swal({
                        title: data.message,
                        type: 'success',
                        showCancelButton: true,
                        confirmButtonText: "@lang('Exit to list of items')",
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonText: "@lang('Continue editing')",
                        cancelButtonClass: 'btn btn-light',
                    }).then((confirm) => {
                        if(confirm.value){
                            window.location.href = url;
                        }else{
                            if (data.formAction){
                                form.attr('action',data.formAction);
                                form.prepend('<input name="_method" type="hidden" value="PUT">');
                            }
                        }
                    });
                    form.find('fieldset').attr('disabled', true);
                }).fail((xhr) => {
                    let data = xhr.responseJSON;
                    swal({
                        title: data.message,
                        type: 'error',
                        confirmButtonClass: 'btn btn-primary',
                    })                    
                    //console.log(data);
                }).always((xhr, type, status) => {

                    let response = xhr.responseJSON || status.responseJSON,
                        errors = response.errors || [];
                        console.log(errors);
                    form.find('.field').each((i, el) => {
                        let field = $(el);
                        field.removeClass('is-invalid');
                        container = field.closest('.form-group'),
                            elem = $('<label class="message">');

                        container.find('label.message').remove();

                        if(errors[field.attr('name')]){
                            field.addClass('is-invalid');
                            field.find('input').addClass('is-invalid');
                            errors[field.attr('name')].forEach((msg) => {
                                elem.clone().addClass('validation-invalid-label').html(msg).appendTo(container);
                            });
                        }

                    });
                });
        };

</script>
