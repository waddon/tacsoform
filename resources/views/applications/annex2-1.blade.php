<h2 class="text-center">EU TACSO 3 P2P PROGRAMME – P2P ON-DEMAND</h2>
<h1 class="text-center">FULL APPLICATION FORM</h1>
<div class="d-flex progress justify-content-between mt-3 mb-3">
    <img src="/assets/img/disk-edit.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
</div>
<form id="annex2-1" data-next="step-2">
    <input type="hidden" name="form_id" value="f2">
    <h2 class="text-center">1. General Information on the Applicant and Event/Activity</h2>
    <h3 class="text-center">Applicant Information</h3>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q1')}}</p>
        <input type="text" id="p1_q1" name="p1_q1" required placeholder="{{trans('custom.annex1.p1_q1')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q2')}}</p>
        <input type="text" id="p1_q2" name="p1_q2" required placeholder="{{trans('custom.annex1.p1_q2')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q3')}}</p>
        <input type="email" id="p1_q3" name="p1_q3" required placeholder="{{trans('custom.annex1.p1_q3')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q4')}}</p>
        <input type="text" id="p1_q4" name="p1_q4" required placeholder="{{trans('custom.annex1.p1_q4')}}">
    </div>

    <h3 class="text-center">General event/activity information</h3>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q5')}}</p>
        <input type="text" id="p1_q5" name="p1_q5" required placeholder="{{trans('custom.annex1.p1_q5')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q6')}}</p>
        <input type="text" id="p1_q6" name="p1_q6" required placeholder="dd/mm/yyyy">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q7')}}</p>
        <input type="text" id="p1_q7" name="p1_q7" required placeholder="{{trans('custom.annex1.p1_q7')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q8')}}</p>
        <input type="text" id="p1_q8" name="p1_q8" required placeholder="{{trans('custom.annex1.p1_q8')}}">
    </div>
    <div class="form-group">
        <p>{{trans('custom.annex1.p1_q9')}}</p>
        <input type="number" id="p1_q9" name="p1_q9" required placeholder="{{trans('custom.annex1.p1_q9')}}">
    </div>
    <h3 class="text-center">Categories of Services Required</h3>
    <table>
        <thead>
            <tr>
                <td>Categories of Services Required</td>
                <td>Yes</td>
                <td>No</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_1')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_1_1" name="p1_q10_1" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_1_2" name="p1_q10_1" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_2')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_2_1" name="p1_q10_2" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_2_2" name="p1_q10_2" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_3')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_3_1" name="p1_q10_3" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_3_2" name="p1_q10_3" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_4')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_4_1" name="p1_q10_4" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_4_2" name="p1_q10_4" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_5')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_5_1" name="p1_q10_5" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_5_2" name="p1_q10_5" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_6')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_6_1" name="p1_q10_6" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_6_2" name="p1_q10_6" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_7')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_7_1" name="p1_q10_7" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_7_2" name="p1_q10_7" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_8')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_8_1" name="p1_q10_8" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_8_2" name="p1_q10_8" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_9')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_9_1" name="p1_q10_9" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_9_2" name="p1_q10_9" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_10')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_10_1" name="p1_q10_10" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_10_2" name="p1_q10_10" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_11')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_11_1" name="p1_q10_11" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_11_2" name="p1_q10_11" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_12')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_12_1" name="p1_q10_12" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_12_2" name="p1_q10_12" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_13')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_13_1" name="p1_q10_13" value="Yes" required></td>
                <td class="text-center"><input type="radio" id="p1_q10_13_2" name="p1_q10_13" value="No" required></td>
            </tr>
            <tr>
                <td>{{trans('custom.annex1.p1_q10_14')}}</td>
                <td class="text-center"><input type="radio" id="p1_q10_14_1" name="p1_q10_14" value="Yes" required data-show="#p1_q10_14_2_1_add" data-req="[name=p1_q10_14_3]"></td>
                <td class="text-center"><input type="radio" id="p1_q10_14_2" name="p1_q10_14" value="No" required></td>
            <tr id="p1_q10_14_2_1_add">
                <td colspan="3"><input type="text" id="p1_q10_14_3" name="p1_q10_14_3" placeholder="{{trans('custom.annex1.p1_q10_14_3')}}" class="mt-1"></td>
            </tr>
            </tr>
        </tbody>
    </table>
    <div class="d-flex justify-content-between mt-3">
        <div class="d-flex align-items-center"></div>
        <div class="d-flex align-items-center">
            <span class="mr-2">NEXT PAGE</span>
            <button class="btn btn_arrow_r btn_darkblue"></a>
        </div>
    </div>
</form>
