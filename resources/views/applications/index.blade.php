@extends('layouts.layout')

@section('content')
<style>
    .application h1, .annex1{
        margin-bottom: 2.5rem;
    }
    .annex1, .annex2{
        font-size: 20px;
        color: #2a63b2;
    }
    .application img{
        margin-right: 1.25rem;
    }
    .disabled-color{
        color: #babdc2;
    }
</style>
    @foreach(Auth::user()->applications as $key => $application)
        <div class="card application mb-3" style="padding: 50px;">
            <h1 class="text-center">APPLICATION FROM {{$application->created_at->format('d.m.Y')}}</h1>
            <div class="d-flex justify-content-between align-items-center annex1">
                <div><img src="/assets/img/annexa.png" alt=""> ANNEX 1: Preliminary Application</div>
                @if($application->status_id==1 || $application->status_id==2)
                    <a href="{{route('applications.edit', $application->id)}}">EDIT</a>
                @else
                    <a href="{{route('applications.show', $application->id)}}">VIEW</a>
                @endif
            </div>
            <div class="d-flex justify-content-between align-items-center annex2 @if($application->status_id < 3) disabled-color @endif">
                <div>
                    @if($application->status_id < 3)
                        <img src="/assets/img/annexp.png" alt="">
                    @else
                        <img src="/assets/img/annexa.png" alt="">
                    @endif
                    ANNEX 2: Request Form
                </div>
                @if($application->status_id < 3)
                    <span>CREATE</span>
                @elseif($application->status_id == 3)
                    <a href="{{route('applications.edit', $application->id)}}">CREATE</a>
                @elseif($application->status_id == 4)
                    <a href="{{route('applications.edit', $application->id)}}">EDIT</a>
                @else
                    <a href="{{route('applications.show', $application->id)}}">VIEW</a>
                @endif
            </div>
        </div>
    @endforeach
    @if (Auth::user()->applications->where('status_id','!=',5)->count() == 0)
        <div class="d-flex align-items-center justify-content-end">
            <span class="mr-2">Create New Application</span>
            <a href="{{route('applications.create')}}" class="btn btn_plus btn_darkblue"></a>
        </div>        
    @endif
@endsection

