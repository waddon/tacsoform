<h2 class="text-center">EU TACSO 3 P2P PROGRAMME – P2P ON-DEMAND</h2>
<h1 class="text-center">FULL APPLICATION FORM</h1>
<div class="d-flex progress justify-content-between mt-3 mb-3">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-edit.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
</div>
<form id="annex2-3" data-back="step-2" data-next="step-4">
    <input type="hidden" name="form_id" value="f4">
    <h2 class="text-center">3. Event/Activity Budget / Requested Support</h2>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p3_q1')}}</h3>
        <p><em>See attached separate Excel and fill-in the anticipated costs you expect to be incurred for organisation of your event/activity.</em></p>
        <p><em>In case you are requesting support for a non-event type (such as coaching, mentoring, internship, etc.) or your request incl. non-event type support element, please describe bellow anticipated cost categories as well as anticipated costs (in EUR), as well as any provision of co-financing.</em></p>
        <input type="text" id="p3_q1" name="p3_q1" required>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p3_q2')}}</h3>
        <p><em>Detailed description of services that will be covered by your organisation/activities that will be undertaken by your organisation in relation to the event (number and profile of staff that will be involved, equipment / venue provided, etc.).</em></p>
        <p><em>Description of involvement of any partner organisation.</em></p>
        <textarea id="p3_q2" name="p3_q2" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <h2 class="text-center">{{trans('custom.annex1.p3_q3')}}</h2>
    <div class="form-group">
        <p><em>Description of specific follow-up actions for experience sharing and provision of feedback.</em></p>
        <textarea id="p3_q3" name="p3_q3" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <h2 class="text-center">{{trans('custom.annex1.p3_q4')}}</h2>
    <div class="form-group">
        <p><em>Description of activities planned for increasing the visibility of the event.</em></p>
        <textarea id="p3_q4" name="p3_q4" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <h2 class="text-center">{{trans('custom.annex1.p3_q5')}}</h2>
    <div class="form-group">
        <p><em>Description of specific outputs such as reports, declarations, position papers, joint statements, etc.</em></p>
        <textarea id="p3_q5" name="p3_q5" required maxlength="1200"></textarea>
        <p class="description">(Max 1200 symbols)</p>
    </div>

    <div class="form-group">
        @if($downloadFile = \App\Option::getOption('downloadFile'))
        <p>Download this <a href="{{$downloadFile}}" download>document</a></p>
        @endif
        <input type="file" id="p3_q6" name="p3_q6" required>
        <p><em id="old-file"></em></p>
    </div>

    <div class="d-flex justify-content-between mt-3">
        <div class="d-flex align-items-center">
            <a class="btn btn_arrow_r btn_darkblue pageback"></a>
            <span class="ml-2">PREVIOUS PAGE</span>
        </div>
        <div class="d-flex align-items-center">
            <span class="mr-2">NEXT PAGE</span>
            <button class="btn btn_arrow_r btn_darkblue"></button>
        </div>
    </div>
</form>
