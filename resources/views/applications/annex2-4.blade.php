<h1 class="text-center">AGENDA</h1>
<div class="d-flex progress justify-content-between mt-3 mb-3">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-edit.png" alt="">
</div>
<p><em>Use the below format/fields to present the agenda of the event. Please adjust it according to the needs of your event (study visit, forums, mentoring, etc.), and delete or add as many days as proposed.</em></p>
<form id="annex2-5" data-back="step-3">
    <div class="days">
        @if(is_array($model->agenda) || is_object($model->agenda))
            @foreach($model->agenda as $key => $day)
                <div class="day mb-3">
                    <h2 class="text-center day-title" contenteditable="true">{!!$day->title!!}</h2>
                    <table class="agenda">
                        <thead>
                            <tr>
                                <td>Time</td>
                                <td>Session Title/Brief Description</td>
                                <td>Speaker/Presenter/Moderator</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody class="tbody-part1">{!!$day->part1!!}</tbody>
                        <tbody class="tbody-part1-button">@include("applications.tbody-part1-button")</tbody>
                        <tbody class="tbody-break">{!!$day->break!!}</tbody>
                        <tbody class="tbody-part2">{!!$day->part2!!}</tbody>
                        <tbody class="tbody-part2-button">@include("applications.tbody-part2-button")</tbody>
                        <tbody class="tbody-lunch">{!!$day->lunch!!}</tbody>
                        <tbody class="tbody-part3">{!!$day->part3!!}</tbody>
                        <tbody class="tbody-part3-button">@include("applications.tbody-part3-button")</tbody>
                        <tbody class="tbody-remove">@include("applications.tbody-remove")</tbody>
                    </table>
                </div>
            @endforeach
        @else
            @include("applications.new-day")
        @endif
    </div>

    <div class="d-flex align-items-center justify-content-end create-day-wrapper">
        <span class="mr-2">Create new day</span>
        <button type="button" class="btn btn_plus btn_darkblue create-day"></button>
    </div>

    <div class="d-flex justify-content-between mt-3">
        <div class="d-flex align-items-center">
            <a class="btn btn_arrow_r btn_darkblue pageback"></a>
            <span class="ml-2">PREVIOUS PAGE</span>
        </div>
        <div class="d-flex align-items-center">
            <button class="btn btn_green" style="width: auto;">SUBMIT</button>
        </div>
    </div>
</form>

<div style="display:none;">
    <div id="template-new-day">
        @include("applications.new-day")
    </div>
</div>