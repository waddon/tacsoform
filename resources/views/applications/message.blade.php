@extends('layouts.layout')

@section('content')
<div class="text-center">
    <img class="mb-3" src="/assets/img/message-logo.jpg">
    <h1 class="text-center">{{ $message }}</h1>
</div>
@endsection

