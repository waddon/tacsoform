<h1 class="text-center">METHODOLOGY</h1>
<div class="d-flex progress justify-content-between mt-3 mb-3">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-edit.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
</div>
<form id="annex2-4" data-back="step-3" data-next="step-5">
    <h3 class="text-center">4. INVOLVEMENT OF YOUR ORGANISATION</h3>
    <div class="form-group">
        <p><em>Detailed description of services that will be covered by your organisation/activities that will be undertaken by your organisation in relation to the event (number and profile of staff that will be involved, equipment / venue provided, etc.)<br>Description of involvement of any partner organisation</em></p>
        <textarea type="text" id="p4_q1" name="p4_q1" required></textarea>
    </div>

    <h3 class="text-center">5. OUTREACH OF THE FOLLOW-UP ACTIONS</h3>
    <div class="form-group">
        <p><em>Detailed description of specific follow-up actions for experience sharing and provision of feedback.</em></p>
        <textarea type="text" id="p4_q2" name="p4_q2" required></textarea>
    </div>

    <h3 class="text-center">6. ACCOMPANYING VISIBILITY ACTIVITIES</h3>
    <div class="form-group">
        <p><em>Description of activities planned for increasing the visibility of the event</em></p>
        <textarea type="text" id="p4_q3" name="p4_q3" required></textarea>
    </div>

    <h3 class="text-center">7. OUTPUTS</h3>
    <div class="form-group">
        <p><em>Description of specific outputs such as reports, declarations, position papers, joint statements, etc.</em></p>
        <textarea type="text" id="p4_q4" name="p4_q4" required></textarea>
    </div>

    <div class="d-flex justify-content-between mt-3">
        <div class="d-flex align-items-center">
            <a class="btn btn_arrow_r btn_darkblue pageback"></a>
            <span class="ml-2">PREVIOUS PAGE</span>
        </div>
        <div class="d-flex align-items-center">
            <span class="mr-2">NEXT PAGE</span>
            <button class="btn btn_arrow_r btn_darkblue"></button>
        </div>
    </div>
</form>
