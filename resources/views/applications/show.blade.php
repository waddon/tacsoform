@extends('layouts.layout')

@section('content')
<style>
    .w-100{width: 100%!important;max-width: 100%!important;}
    @media screen and (max-width: 767px){
        .tab-label-wrap {
            flex-wrap: wrap;
        }
    }
</style>
<a href="{{route('applications.index')}}" class="mb-3" style="display:block;">Back to Main Page</a>
<div class="card w-100">
        <div class="card-header tab-label-wrap">
            <a class="tab-label active" data-id="tab-f1">Preliminary Application</a>
            @if($model->status_id > 3)
            <a class="tab-label" data-id="tab-f2">General Information on the Applicant and Event/Activity</a>
            <a class="tab-label" data-id="tab-f3">EVENT/ACTIVITY INFORMATION</a>
            <a class="tab-label" data-id="tab-f4">Event/Activity Budget / Requested Support</a>
            <a class="tab-label" data-id="tab-agenda">Agenda</a>
            @endif
        </div>
        <div class="card-body" style="padding: 15px;">
            <div id="tab-f1" class="tab-content mt-5">
                <table class="table table-striped">
                    <thead><tr><td>Parameter</td><td>Value</td></tr></thead>
                    <tbody>
                        {!!$model->showApplicationForm('f1')!!}
                    </tbody>
                </table>

            </div>
            @if($model->status_id > 3)
                <div id="tab-f2" class="tab-content mt-5" style="display:none">
                    <table class="table table-striped">
                        <thead><tr><td>Parameter</td><td>Value</td></tr></thead>
                        <tbody>
                            {!!$model->showApplicationForm('f2')!!}
                        </tbody>
                    </table>
                </div>
                <div id="tab-f3" class="tab-content mt-5" style="display:none">
                    <table class="table table-striped">
                        <thead><tr><td>Parameter</td><td>Value</td></tr></thead>
                        <tbody>
                            {!!$model->showApplicationForm('f3')!!}
                        </tbody>
                    </table>
                </div>
                <div id="tab-f4" class="tab-content mt-5" style="display:none">
                    <table class="table table-striped">
                        <thead><tr><td>Parameter</td><td>Value</td></tr></thead>
                        <tbody>
                            {!!$model->showApplicationForm('f4')!!}
                        </tbody>
                    </table>
                    @if(isset($model->data->file))
                    <div class="mt-3 mb-3">
                        Uploaded file: <a href="/storage/{{ $model->data->file->url ?? '#' }}" download>{{ $model->data->file->name ?? 'filename' }}</a>                        
                    </div>
                    @endif
                </div>
                <div id="tab-agenda" class="tab-content mt-5" style="display:none">
                    <div id="agenda-wrapper">
                        @if(is_array($model->agenda))
                            @foreach($model->agenda as $key => $row)
                                <h1 class="text-center">{{$row->title}}</h1>
                                <div class="table-responsive mb-3"><table class="table table-striped"><thead><tr><td>Time</td><td>Session Title/Brief Description</td><td>Speaker/Presenter/Moderator</td><td></td></tr></thead>
                                {!!$row->part1!!}
                                {!!$row->break!!}
                                {!!$row->part2!!}
                                {!!$row->lunch!!}
                                {!!$row->part3!!}
                                </table></div>
                            @endforeach
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    $('#agenda-wrapper').find('*').removeAttr('contenteditable');
    $('#agenda-wrapper').find('button').remove();

    $(document).on('click','.tab-label:not(.active)',function(e){
        e.preventDefault();
        $('.tab-label').removeClass('active');
        $(this).addClass('active');
        $('.tab-content').css('display','none');
        $('#'+$(this).attr('data-id')).fadeIn();
    });
});
</script>
@endsection
