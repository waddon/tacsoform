            <div class="day mb-3">
                <h2 class="text-center day-title" contenteditable="true">Day _ - Date</h2>
                <table class="agenda">
                    <thead>
                        <tr>
                            <td>Time</td>
                            <td>Session Title/Brief Description</td>
                            <td>Speaker/Presenter/Moderator</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody class="tbody-part1"></tbody>
                    <tbody class="tbody-part1-button">@include("applications.tbody-part1-button")</tbody>
                    <tbody class="tbody-break"><tr><td contenteditable="true"></td><td colspan="3">Break</td></tr></tbody>
                    <tbody class="tbody-part2"></tbody>
                    <tbody class="tbody-part2-button">@include("applications.tbody-part2-button")</tbody>
                    <tbody class="tbody-lunch"><tr><td contenteditable="true"></td><td colspan="3">Lunch</td></tr></tbody>
                    <tbody class="tbody-part3"></tbody>
                    <tbody class="tbody-part3-button">@include("applications.tbody-part3-button")</tbody>
                    <tbody class="tbody-remove">@include("applications.tbody-remove")</tbody>
                </table>
