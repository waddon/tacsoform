<h2 class="text-center">EU TACSO 3 P2P PROGRAMME – P2P ON-DEMAND</h2>
<h1 class="text-center">FULL APPLICATION FORM</h1>
<div class="d-flex progress justify-content-between mt-3 mb-3">
    <img src="/assets/img/disk-complete.png" alt="">
    <img src="/assets/img/disk-edit.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
    <img src="/assets/img/disk-empty.png" alt="">
</div>
<form id="annex2-2"  data-back="step-1" data-next="step-3">
    <input type="hidden" name="form_id" value="f3">
    <h2 class="text-center">2. EVENT/ACTIVITY INFORMATION</h2>
    <h3 class="text-center">{{trans('custom.annex1.p2_q1')}}</h3>
    <p><em> •   Description of the rationale for submitting this proposal<br>
        •   Description of relevance to the EU TACSO 3 project and P2P programme objectives<br>
        •   Where the event is a part of larger intervention/project, explain how it fits with that intervention or any other possibly planned project. Please specify potential synergy with other interventions.</em></p>
    <div class="form-group">
        <textarea type="text" id="p2_q1" name="p2_q1" required maxlength="2800"></textarea>
        <p class="description">(Max 2800 symbols)</p>
    </div>

    <h3 class="text-center">{{trans('custom.annex1.p2_q2')}}</h3>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_1')}}</h3>
        <p><em>Broad statement about what you aim to achieve overall (medium-term effect(s) of the intervention focusing on behavioural and institutional changes, if any, resulting from the intervention).</em></p>
        <textarea type="text" id="p2_q2_1" name="p2_q2_1" required maxlength="2000"></textarea>
        <p class="description">(Max 2000 symbols)</p>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_2')}}</h3>
        <p><em>Description of what is specifically sought and specific expected outcomes (short-term effect(s) of the intervention focusing on behavioural and institutional changes, if any, resulting from the intervention).</em></p>
        <textarea type="text" id="p2_q2_2" name="p2_q2_2" required maxlength="2000"></textarea>
        <p class="description">(Max 2000 symbols)</p>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_3')}}</h3>
        <p><em>Description of the target group sector or theme of work (e.g. women, youth, gender, etc.) and countries in which they are based, approach followed to select these participants and relevance of the event/activity to the needs of the specific target groups.</em></p>
        <textarea type="text" id="p2_q2_3" name="p2_q2_3" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_4')}}</h3>
        <p><em>Description of the relevance of the proposed priority area/ topic (selected in the Preliminary application form) to the objectives of EU TACSO 3 project, Capacity Development and P2P Programme, EU Civil Society Guidelines and particular needs and constraints of the target group.</em></p>
        <textarea type="text" id="p2_q2_4" name="p2_q2_4" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_5')}}</h3>
        <p><em>Description of format (training, study visit, workshop, forum, networking, coaching, mentoring, job shadowing etc.), type of activities planned, reasoning behind selection of the specific format and advantages of this type of event.</em></p>
        <textarea type="text" id="p2_q2_5" name="p2_q2_5" required maxlength="1600"></textarea>
        <p class="description">(Max 1600 symbols)</p>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_6')}}</h3>
        <p><em>Place of venue(s) of the event(s) and geographical area targeted.</em></p>
        <input type="text" id="p2_q2_6" name="p2_q2_6" required>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_7')}}</h3>
        <p><em>Please specify if other than English.</em></p>
        <input type="text" id="p2_q2_7" name="p2_q2_7" required>
    </div>
    <div class="form-group">
        <h3>{{trans('custom.annex1.p2_q2_8')}}</h3>
        <p><em>Starting period and duration of the event.</em></p>
        <input type="text" id="p2_q2_8" name="p2_q2_8" required>
    </div>

    <div class="d-flex justify-content-between mt-3">
        <div class="d-flex align-items-center">
            <a class="btn btn_arrow_r btn_darkblue pageback"></a>
            <span class="ml-2">PREVIOUS PAGE</span>
        </div>
        <div class="d-flex align-items-center">
            <span class="mr-2">NEXT PAGE</span>
            <button class="btn btn_arrow_r btn_darkblue"></button>
        </div>
    </div>
</form>
