@extends('layouts.layout')

@section('content')
    <div class="infopage-wrapper">
        <!-- tabs -->
            @if( isset($infopages) )
                @foreach($infopages as $key => $infopage)
                    <div id="infopage-{{$key}}" class="infopage-content" style="{{ $key > 0 ? 'display: none;' : '' }}">
                        {!! $infopage->content !!}
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <button class="btn btn_arrow_r btn_darkblue infopage-next pr-4" data-next="infopage-{{$key+1}}">Next</button>
                        </div>
                    </div>
                @endforeach
                <script>
                    $('.infopage-next').click(function(e){
                        e.preventDefault();
                        showtab($(this).attr('data-next'));
                    });
                    function showtab(idtab){
                        $('.infopage-content').css('display','none');
                        $('#'+idtab).fadeIn();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                </script>
            @endif
        <!-- /tabs -->
        <!-- form -->
            <div id="infopage-{{isset($key) ? ($key+1) : 0}}" class="infopage-content" style="{{ isset($key) ? 'display: none;' : '' }}">
                <a href="{{route('applications.index')}}" class="mb-3" style="display:block;">Back to Main Page</a>
                <p class="text-center annex-title">Annex 1: Preliminary Application</p>
                <div class="card" style="padding: 50px;">
                    <h1 class="text-center">Preliminary Application</h1>
                    <form id="annex1">
                        <input type="hidden" name="form_id" value="f1">
                        <div class="form-group">
                            <p>{{trans('custom.annex1.q1')}}</p>
                            <input type="text" id="q1" name="q1" placeholder="Enter your name" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q2')}}</p>
                            <div class="mb-2">
                                <input type="radio" id="q2_1" name="q2" required value="{{trans('custom.annex1.q2_1')}}" data-show="#q2_1_0" data-req="[name=q2_1]">
                                <label for="q2_1"> {{trans('custom.annex1.q2_1')}}</label>
                                <div class="pl-4 mt-2" id="q2_1_0">
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_1" name="q2_1" value="{{trans('custom.annex1.q2_1_1')}}">
                                        <label for="q2_2_1"> {{trans('custom.annex1.q2_1_1')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_2" name="q2_1" value="{{trans('custom.annex1.q2_1_2')}}">
                                        <label for="q2_2_2"> {{trans('custom.annex1.q2_1_2')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_3" name="q2_1" value="{{trans('custom.annex1.q2_1_3')}}">
                                        <label for="q2_2_3"> {{trans('custom.annex1.q2_1_3')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_4" name="q2_1" value="{{trans('custom.annex1.q2_1_4')}}">
                                        <label for="q2_2_4"> {{trans('custom.annex1.q2_1_4')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_5" name="q2_1" value="{{trans('custom.annex1.q2_1_5')}}">
                                        <label for="q2_2_5"> {{trans('custom.annex1.q2_1_5')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_6" name="q2_1" value="{{trans('custom.annex1.q2_1_6')}}">
                                        <label for="q2_2_6"> {{trans('custom.annex1.q2_1_6')}}</label>
                                    </div>
                                    <div class="mb-2">
                                        <input type="radio" id="q2_1_7" name="q2_1" value="{{trans('custom.annex1.q2_1_7')}}">
                                        <label for="q2_2_7"> {{trans('custom.annex1.q2_1_7')}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_2" name="q2" required value="{{trans('custom.annex1.q2_2')}}">
                                <label for="q2_2"> {{trans('custom.annex1.q2_2')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_3" name="q2" required value="A National Resource Centre">
                                <label for="q2_3"> {{trans('custom.annex1.q2_3')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q2_4" name="q2" required value="Other (Please specify)" data-show="#q2_4_1" data-req="#q2_4_1">
                                <label for="q2_4"> {{trans('custom.annex1.q2_4')}}</label>
                            </div>
                            <input type="text" id="q2_4_1" name="q2_4_1" placeholder="{{trans('custom.annex1.q2_4_1')}}" class="mt-1">
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q3')}}</p>
                            <input type="text" id="q3" name="q3" placeholder="{{trans('custom.annex1.q3')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q4')}}</p>
                            <input type="text" id="q4" name="q4" placeholder="Position / role" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q5')}}</p>
                            <input type="text" id="q5" name="q5" placeholder="http://tacso.eu" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q6')}}</p>
                            <input type="email" id="q6" name="q6" placeholder="Enter email" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q7')}}</p>
                            <input type="text" id="q7" name="q7" placeholder="Enter number" required pattern="[0-9/(/)-/+]*">
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q8')}}</p>
                            <table>
                                <thead>
                                    <tr>
                                        <td class="text-center" style="width: 50%">{{trans('custom.annex1.q8_1')}}</td>
                                        <td class="text-center" style="width: 50%">{{trans('custom.annex1.q8_2')}}</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><input type="radio" id="q8_1_1" name="q8_1" required value="{{trans('custom.annex1.q8_1_1')}}"><label for="q8_1_1">{{trans('custom.annex1.q8_1_1')}}</label></td>
                                        <td><input type="radio" id="q8_2_1" name="q8_2" required value="{{trans('custom.annex1.q8_2_1')}}"><label for="q8_2_1">{{trans('custom.annex1.q8_2_1')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_2" name="q8_1" required value="{{trans('custom.annex1.q8_1_2')}}"><label for="q8_1_2">{{trans('custom.annex1.q8_1_2')}}</label></td>
                                        <td><input type="radio" id="q8_2_2" name="q8_2" required value="{{trans('custom.annex1.q8_2_2')}}"><label for="q8_2_2">{{trans('custom.annex1.q8_2_2')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_3" name="q8_1" required value="{{trans('custom.annex1.q8_1_3')}}"><label for="q8_1_3">{{trans('custom.annex1.q8_1_3')}}</label></td>
                                        <td><input type="radio" id="q8_2_3" name="q8_2" required value="{{trans('custom.annex1.q8_2_3')}}"><label for="q8_2_3">{{trans('custom.annex1.q8_2_3')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_4" name="q8_1" required value="{{trans('custom.annex1.q8_1_4')}}"><label for="q8_1_4">{{trans('custom.annex1.q8_1_4')}}</label></td>
                                        <td><input type="radio" id="q8_2_4" name="q8_2" required value="{{trans('custom.annex1.q8_2_4')}}"><label for="q8_2_4">{{trans('custom.annex1.q8_2_4')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_5" name="q8_1" required value="{{trans('custom.annex1.q8_1_5')}}"><label for="q8_1_5">{{trans('custom.annex1.q8_1_5')}}</label></td>
                                        <td><input type="radio" id="q8_2_5" name="q8_2" required value="{{trans('custom.annex1.q8_2_5')}}"><label for="q8_2_5">{{trans('custom.annex1.q8_2_5')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_6" name="q8_1" required value="{{trans('custom.annex1.q8_1_6')}}"><label for="q8_1_6">{{trans('custom.annex1.q8_1_6')}}</label></td>
                                        <td><input type="radio" id="q8_2_6" name="q8_2" required value="{{trans('custom.annex1.q8_2_6')}}"><label for="q8_2_6">{{trans('custom.annex1.q8_2_6')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td><input type="radio" id="q8_1_7" name="q8_1" required value="{{trans('custom.annex1.q8_1_7')}}" data-show="#q8_1_7_1" data-req="#q8_1_7_1"><label for="q8_1_7">{{trans('custom.annex1.q8_1_7')}}</label>
                                        <input type="text" id="q8_1_7_1" name="q8_1_7_1" placeholder="{{trans('custom.annex1.q8_1_7_1')}}" class="mt-1">
                                        </td>
                                        <td><input type="radio" id="q8_2_7" name="q8_2" required value="{{trans('custom.annex1.q8_2_7')}}"><label for="q8_2_7">{{trans('custom.annex1.q8_2_7')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="radio" id="q8_2_8" name="q8_2" required value="{{trans('custom.annex1.q8_2_8')}}"><label for="q8_2_8">{{trans('custom.annex1.q8_2_8')}}</label></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="radio" id="q8_2_9" name="q8_2" required value="{{trans('custom.annex1.q8_2_9')}}" data-show="#q8_2_9_1" data-req="#q8_2_9_1"><label for="q8_2_9">{{trans('custom.annex1.q8_2_9')}}</label>
                                        <input type="text" id="q8_2_9_1" name="q8_2_9_1" placeholder="{{trans('custom.annex1.q8_2_9_1')}}" class="mt-1">
                                        </td>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q9')}}</p>
                            <textarea type="text" id="q9" name="q9" placeholder="{{trans('custom.annex1.q9')}}" required  maxlength="1200"></textarea>
                            <p class="description">(Max 1200 symbols)</p>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q10')}}</p>
                            <div class="mb-2">
                                <input type="radio" id="q10_1" name="q10" required value="{{trans('custom.annex1.q10_1')}}">
                                <label for="q10_1"> {{trans('custom.annex1.q10_1')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q10_2" name="q10" required value="{{trans('custom.annex1.q10_2')}}">
                                <label for="q10_2"> {{trans('custom.annex1.q10_2')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q10_3" name="q10" required value="{{trans('custom.annex1.q10_3')}}">
                                <label for="q10_3"> {{trans('custom.annex1.q10_3')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q10_4" name="q10" required value="{{trans('custom.annex1.q10_4')}}">
                                <label for="q10_4"> {{trans('custom.annex1.q10_4')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q10_5" name="q10" required value="{{trans('custom.annex1.q10_5')}}">
                                <label for="q10_5"> {{trans('custom.annex1.q10_5')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q10_6" name="q10" required value="{{trans('custom.annex1.q10_6')}}">
                                <label for="q10_6"> {{trans('custom.annex1.q10_6')}}</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q11')}}</p>
                            <input type="text" id="q11" name="q11" placeholder="{{trans('custom.annex1.q11')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q12')}}</p>
                            <input type="text" id="q12" name="q12" placeholder="{{trans('custom.annex1.q12')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q13')}}</p>
                            <input type="text" id="q13" name="q13" placeholder="{{trans('custom.annex1.q13')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q14')}}</p>
                            <input type="text" id="q14" name="q14" placeholder="{{trans('custom.annex1.q14')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q15')}}</p>
                            <input type="number" id="q15" name="q15" placeholder="{{trans('custom.annex1.q15')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q16')}}</p>
                            <div class="mb-2">
                                <input type="radio" id="q16_1" name="q16" required value="{{trans('custom.annex1.q16_1')}}">
                                <label for="q16_1">{{trans('custom.annex1.q16_1')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q16_2" name="q16" required value="{{trans('custom.annex1.q16_2')}}">
                                <label for="q16_2">{{trans('custom.annex1.q16_2')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q16_3" name="q16" required value="{{trans('custom.annex1.q16_3')}}">
                                <label for="q16_3">{{trans('custom.annex1.q16_3')}}</label>
                            </div>
                            <div class="mb-2">
                                <input type="radio" id="q16_4" name="q16" required value="{{trans('custom.annex1.q16_4')}}" data-show="#q16_4_1" data-req="#q16_4_1">
                                <label for="q16_4">{{trans('custom.annex1.q16_4')}}</label>
                            </div>
                            <input type="text" id="q16_4_1" name="q16_4_1" placeholder="{{trans('custom.annex1.q16_4')}}" class="mt-1">
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q17')}}</p>
                            <input type="text" id="q17" name="q17" placeholder="{{trans('custom.annex1.q17')}}" required>
                        </div>

                        <div class="form-group">
                            <p>{{trans('custom.annex1.q18')}}</p>
                            <input type="text" id="q18" name="q18" placeholder="{{trans('custom.annex1.q18')}}" required>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn_green btn-send">SEND</button>
                        </div>

                    </form>
                </div>
            </div>
        <!-- /form -->
    </div>
@endsection

@section('scripts')
<script>
$(document).ready(function(){
    var data = {};
    @if($model->annex1)
        data = JSON.parse('{!!json_encode($model->annex1)!!}');
        fillFields();
        showhide();
    @else
        showhide();
    @endif

    $('#annex1').submit(function(e){
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{route("applications.approve",$model->id)}}',
            data: new FormData(this),
            dataType: "JSON",
            processData: false,
            contentType: false,
            success: function(response){
                console.log('Sent for approving');
                window.location.href = "{{route('home')}}";
            },
            error: function(response){
                console.log(response);
            }
        });
    });


    $('input, textarea, select').change(function(){
        saveData();
        showhide();
    });

    function showhide(){
        $("[data-show]").each(function(){
            var element = $( this );
            if (element.prop('checked')){
                $( element.attr('data-show') ).show();
                $( element.attr('data-req') ).prop('required',true);
            } else {
                $( element.attr('data-show') ).hide();
                $( element.attr('data-req') ).removeAttr('required').val('');
            }
        });
    };

    function fillFields(){
        for(var p in data) {
            element = '#'+ p;
            if ($('#annex1 input[type="text"], #annex1 input[type="email"], #annex1 input[type="number"], #annex1 select').is(element)) { $(element).val(data[p]);};
            if ($('#annex1 textarea').is(element)) { if (data[p]!=null) $(element).val(data[p].replace(/\\n/g,'\n').replace(/\\r/g,'\r') );};
            if ($('#annex1 input[type="radio"], #annex1 input[type="checkbox"]').is(element)) {
                $(element).attr("checked", !!parseInt(data[p]) );
                if ($(element).prop('checked')) $(element).parent().addClass('checked');
            };
        };
    }

    function saveData() {
        $('#annex1 input[type="text"], #annex1 input[type="email"], #annex1 input[type="number"], #annex1 select').each(function(){
            data[ $(this).attr("id") ] = $(this).val()
        });
        $('#annex1 textarea').each(function(){
            data[ $(this).attr("id") ] = $(this).val().replace(/\n/g,'\\n').replace(/\r/g,'\\r');
        });
        $('#annex1 input[type="radio"], #annex1 input[type="checkbox"]').each(function(){
            if ( $( this ).prop("checked") ) {value = 1;} else {value = 0;}
            data[ $(this).attr("id") ] = value;
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var postData = {
            'annex1': data,
        }
        $.ajax({
            type: 'POST',
            url: '{{route("applications.postupdate",$model->id)}}',
            data: postData,
            success: function(response){
                console.log('Saved');
            },
            error: function(response){
                console.log(response);
            }
        });

        console.log( postData );
    }
});
</script>
@endsection
