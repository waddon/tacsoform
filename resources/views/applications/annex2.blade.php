@extends('layouts.layout')

@section('content')
    <div class="infopage-wrapper">
        <!-- tabs -->
            @if( isset($infopages) )
                @foreach($infopages as $key => $infopage)
                    <div id="infopage-{{$key}}" class="infopage-content" style="{{ $key > 0 ? 'display: none;' : '' }}">
                        {!! $infopage->content !!}
                        <div class="d-flex align-items-center justify-content-center mt-3">
                            <button class="btn btn_arrow_r btn_darkblue infopage-next pr-4" data-next="infopage-{{$key+1}}">Next</button>
                        </div>
                    </div>
                @endforeach
                <script>
                    $('.infopage-next').click(function(e){
                        e.preventDefault();
                        showtab($(this).attr('data-next'));
                    });
                    function showtab(idtab){
                        $('.infopage-content').css('display','none');
                        $('#'+idtab).fadeIn();
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                    }
                </script>
            @endif
        <!-- /tabs -->
        <!-- form -->
            <div id="infopage-{{isset($key) ? ($key+1) : 0}}" class="infopage-content" style="{{ isset($key) ? 'display: none;' : '' }}">
                <a href="{{route('applications.index')}}" class="mb-3" style="display:block;">Back to Main Page</a>
                <p class="text-center annex-title">Annex 2: Request Form</p>
                <div class="card form-wrapper" style="padding: 50px;">
                    <div id="step-1" class="tab-content mt-5">
                        @include('applications.annex2-1')
                    </div>
                    <div id="step-2" class="tab-content mt-5" style="display: none;">
                        @include('applications.annex2-2')
                    </div>
                    <div id="step-3" class="tab-content mt-5" style="display: none;">
                        @include('applications.annex2-3')
                    </div>
                    <div id="step-4" class="tab-content mt-5" style="display: none;">
                        @include('applications.annex2-4')
                    </div>
                </div>
            </div>
        <!-- /form -->
    </div>
@endsection

@section('scripts')
<script>
jQuery(document).ready(function($){
    var data = {};
    var content='';
    @if($model->annex2)
        data = JSON.parse('{!! json_encode($model->annex2) !!}');
        file = JSON.parse('{!! isset($model->data->file) ? json_encode($model->data->file) : "{\"name\":\"\"}" !!}');
        fillFields();
    @endif
        showhide();

    $('form').submit(function(e){
        e.preventDefault();
        if ($(this).attr('id')!='annex2-5'){
            showtab($(this).attr('data-next'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{route("applications.approve",$model->id)}}',
                data: new FormData(this),
                dataType: "JSON",
                processData: false,
                contentType: false,
                success: function(response){
                    console.log('Save one of the forms');
                },
                error: function(response){
                    console.log(response);
                }
            });
        } else {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '{{route("applications.approve",$model->id)}}',
                data: {'status':5},
                success: function(response){
                    console.log('Sent');
                    window.location.href = "{{route('home')}}";
                },
                error: function(response){
                    console.log(response);
                }
            });            
        }
    });
    $('.pageback').click(function(e){
        e.preventDefault();
        showtab($(this).closest('form').attr('data-back'));
    });

    function showtab(idtab){
        $('.tab-content').css('display','none');
        $('#'+idtab).fadeIn();
        $("html, body").animate({ scrollTop: 0 }, "slow");
    }

    $('input, textarea, select').change(function(){
        saveData();
        showhide();
    });

    function fillFields(){
        for(var p in data) {
            element = '#'+ p;
            //console.log(element);
            if ($('.form-wrapper input[type="text"], .form-wrapper input[type="email"], .form-wrapper input[type="number"], .form-wrapper input[type="date"], .form-wrapper select').is(element)) { $(element).val(data[p]);};
            if ($('.form-wrapper textarea').is(element)) { if (data[p]!=null) $(element).val(data[p].replace(/\\n/g,'\n').replace(/\\r/g,'\r') );};
            if ($('.form-wrapper input[type="radio"], .form-wrapper input[type="checkbox"]').is(element)) {
                $(element).attr("checked", !!parseInt(data[p]) );
                if ($(element).prop('checked')) $(element).parent().addClass('checked');
            };
        };
        if (file.name != ''){
            $('#old-file').html(file.name);
            $('#p3_q6').removeAttr('required');
        }
    }

    function saveData() {
        $('.form-wrapper input[type="text"], .form-wrapper input[type="email"], .form-wrapper input[type="number"], .form-wrapper input[type="date"], .form-wrapper select').each(function(){
            data[ $(this).attr("id") ] = $(this).val()
        });
        $('.form-wrapper textarea').each(function(){
            data[ $(this).attr("id") ] = $(this).val().replace(/\n/g,'\\n').replace(/\r/g,'\\r');
        });
        $('.form-wrapper input[type="radio"], .form-wrapper input[type="checkbox"]').each(function(){
            if ( $( this ).prop("checked") ) {value = 1;} else {value = 0;}
            data[ $(this).attr("id") ] = value;
        });
        var agenda = create_json();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var postData = {
            'annex2': data,
            'agenda': agenda,
        }
        $.ajax({
            type: 'POST',
            url: '{{route("applications.postupdate",$model->id)}}',
            data: postData,
            success: function(response){
                console.log('Saved');
            },
            error: function(response){
                console.log(response);
            }
        });
        //console.log( postData );
    }

    $('.form-wrapper').on('focus','td, .day-title',function(){
        content = $(this).text()
    });
    $('.form-wrapper').on('blur','td, .day-title',function(){
        var element = $(this);
        var whitelist = "br";
        if(element.text() != content){
            element.find('*').not(whitelist).each(function() {
                var tempcontent = $(this).contents();
                $(this).replaceWith(tempcontent);
            });
            saveData();
        }
    });

    $('.form-wrapper').on('click','.btn_plus:not(.create-day)',function(){
        element = $(this).closest('table');
        $(element).find('.'+$(this).attr('data-container')).append('@include("applications.new-session")');
        saveData();
    });
    $('.form-wrapper').on('click','.remove-session',function(){
        $(this).closest('tr').remove();
        saveData();
    });

    $('.form-wrapper').on('click','.remove-day',function(){
        $(this).closest('.day').remove();
        saveData();
    });

    $('.form-wrapper').on('click','.create-day',function(){
        $('.days').append( $('#template-new-day').html() );
        saveData();
    });

    function create_json() {
        var object = [];
        $('form#annex2-5 .days .day').each(function(index){
            current_day = $(this);
            var day = {};
            day.title = current_day.find('.day-title').first().html();
            day.part1 = current_day.find('.tbody-part1').first().html();
            day.break = current_day.find('.tbody-break').first().html();
            day.part2 = current_day.find('.tbody-part2').first().html();
            day.lunch = current_day.find('.tbody-lunch').first().html();
            day.part3 = current_day.find('.tbody-part3').first().html();
            object.push(day);
        });
        return object;
    }

    function showhide(){
        $("[data-show]").each(function(){
            var element = $( this );
            if (element.prop('checked')){
                $( element.attr('data-show') ).show();
                $( element.attr('data-req') ).prop('required',true);
            } else {
                $( element.attr('data-show') ).hide();
                $( element.attr('data-req') ).removeAttr('required').val('');
            }
        });
    };

});
</script>
@endsection
