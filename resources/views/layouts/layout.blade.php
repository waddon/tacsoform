@include('layouts.header')

<section id="home_page">
    <div class="container">
        <div class="content-wrapper">
            <div style="width: 100%;">
                @section('content')
                @show
            </div>            
        </div>
        <div class="divider"></div>
    </div>
</section>

@include('layouts.footer')
