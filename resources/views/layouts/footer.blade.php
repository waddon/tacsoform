<footer>
    <div class="container">
        <div id="pageup"></div>
        <div class="row">
            <div class="col1">
                <div class="logo">

                    <img src="/assets/img/footer_img_1.png">
                    <span>Technical Assistance<br>for Civil Society Organisations</span>

                </div>
                <p>TACSO's mission is to increase and improve the capacity and actions of
                    CSOs as well as their democratic role. Through TACSO's capacity building
                    activities, support and assistance, the aim is to achieve a strengthened
                    civil society and to stimulate a civil society-friendly
                    'environment' and culture.</p>
                <ul class="soc">
                    <li><a href="https://www.facebook.com/TACSO3" class="fb"></a></li>                  <li><a href="https://twitter.com/tacso1" class="tw"></a></li>                   <li><a href="https://telegram.org/" class="telegram"></a></li>              </ul>
            </div>
            <div class="col2">
                <p class="title">Contacts</p>
                <address>EU TACSO 3 Project (GDSI) <br />
Str. Blagoj Davkov n.1-4/14<br />
1000 Skopje, North Macedonia</address>
                <a href="tel:+ 389 260 90 684" class="phone">+ 389 260 90 684</a>
                <a href="mailto:welcome@tacso.eu" class="mail">welcome@tacso.eu</a>
            </div>
            <div class="col3">
                <p class="title">Project countries</p>
                <ul class="f_menu">
                    <li><a href="http://tacso.eu/country-list/albania/">Albania</a></li><li><a href="http://tacso.eu/country-list/bosnia/">Bosnia and Herzegovina</a></li><li><a href="http://tacso.eu/country-list/kosovo/">Kosovo<sup>*</sup></a></li><li><a href="http://tacso.eu/country-list/montenegro/">Montenegro</a></li><li><a href="http://tacso.eu/country-list/north-macedonia/">North Macedonia</a></li><li><a href="http://tacso.eu/country-list/serbia/">Serbia</a></li><li><a href="http://tacso.eu/country-list/turkey/">Turkey</a></li>              </ul>
            </div>
        </div>
        <div class="divider"></div>
        <div class="info">
            <div class="col"><img src="/assets/img/footer_img_1.png" alt="" class="fl_left">
<p>The information on this site is subject to a Disclaimer. Privacy Statement. Copyright Notice and Protection of personal data. © European Union, 2019</p>
</div><div class="col"><img src="/assets/img/footer_img_2.png" alt="" class="fl_left"><img src="/assets/img/footer_img_3.png" alt="" class="fl_right">
<p class="center">EU-funded project implemented by the consortium of GDSI Limited and LDK Consultants</p>
</div>      </div>
        <div class="divider"></div>
    </div>
    
    <div class="container">
        <div class="disclaimer">
            <img src="/assets/img/disc-01.png" alt="">
            <p>This website was created and maintained with the financial support of the European Union. Its contents are the sole responsibility of GDSI Limited and do not neccessarily reflect the views of the European Union</p>
        </div>
        <div class="disclaimer">
            <img src="/assets/img/disc-02.png" alt="">
            <p>Kosovo (*) – This designation is without prejudice to positions on status, and is in line with UNSCR 1244/1999 and the ICJ opinion on the Kosovo declaration of independence.</p>
        </div>
    </div>

</footer>

<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"http:\/\/tacso.eu\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<!--script type='text/javascript' src='http://tacso.eu/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.4'></script-->
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jQueryFormStyler/2.0.2/jquery.formstyler.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js?ver=1.0'></script>
<script type='text/javascript' src='/assets/js/main.js'></script>
<!--script type='text/javascript' src='http://tacso.eu/wp-includes/js/wp-embed.min.js?ver=5.2.4'></script-->
<script>
    $('.accordeon .acc-head').on('click',function(){
        $(this).closest('.accordeon').find('.acc-body').not($(this).next()).slideUp(200);
        $(this).next().slideToggle(200);
        $(this).closest('.accordeon').find('.acc-head').not($(this)).removeClass('open');
        $(this).toggleClass('open');
    });
</script>
    @section('scripts')
    @show
</body>
</html>
