
<!DOCTYPE html>
<html lang="en-US" >
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>

    <link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel='stylesheet' id='wp-block-library-css'  href='http://tacso.eu/wp-includes/css/dist/block-library/style.min.css?ver=5.2.4' type='text/css' media='all' />
    <!--link rel='stylesheet' id='contact-form-7-css'  href='http://tacso.eu/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.4' type='text/css' media='all' /-->
    <link rel='stylesheet' id='wp-pagenavi-css'  href='http://tacso.eu/wp-content/plugins/wp-pagenavi/pagenavi-css.css?ver=2.70' type='text/css' media='all' />
    <link rel='stylesheet' id='form_tacso_style-css'  href='/assets/css/main.css' type='text/css' media='all' />
    <link rel='stylesheet' id='form_tacso_style2-css'  href='/assets/css/media.css' type='text/css' media='all' />
    <link rel='stylesheet' id='form_tacso_style3-css'  href='/assets/css/flex.css' type='text/css' media='all' />
    <link rel='stylesheet' id='form_tacso_style4-css'  href='/assets/css/custom.css' type='text/css' media='all' />

    <link rel='stylesheet' id='abvv_tacso_style3-css'  href='https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css?ver=2.0' type='text/css' media='all' />
    <link rel='stylesheet' id='abvv_tacso_style_swiper-css'  href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css?ver=1.0' type='text/css' media='all' />
    <script type='text/javascript' src='/assets/js/jquery-3.3.1.min.js'></script>
    <link rel='https://api.w.org/' href='http://tacso.eu/wp-json/' />
    <link rel="canonical" href="http://tacso.eu/" />
    <link rel="alternate" type="application/json+oembed" href="http://tacso.eu/wp-json/oembed/1.0/embed?url=http%3A%2F%2Ftacso.eu%2F" />
    <link rel="alternate" type="text/xml+oembed" href="http://tacso.eu/wp-json/oembed/1.0/embed?url=http%3A%2F%2Ftacso.eu%2F&#038;format=xml" />
    <link rel="icon" href="http://tacso.eu/wp-content/uploads/2019/04/logo.png" sizes="32x32" />
    <link rel="icon" href="http://tacso.eu/wp-content/uploads/2019/04/logo.png" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="http://tacso.eu/wp-content/uploads/2019/04/logo.png" />
    <meta name="msapplication-TileImage" content="http://tacso.eu/wp-content/uploads/2019/04/logo.png" />
</head>

<body class="home page-template page-template-page-home page-template-page-home-php page page-id-115 wp-custom-logo">
    <header>
        <div id="top_header">
            <div class="container">
                <a href="{{route('home')}}" class="logo">
                    <span class="hide_mob">P2P On-demand<br>support</span>
                    <img src="/assets/img/footer_img_1.png">
                    <span>Technical Assistance<br>for Civil Society Organisations</span>
                </a>
                <button id="open_mob_menu"><span></span><span></span><span></span></button>
                <ul id="menu-header-menu" class="pc_menu">
                    @can('admin')
                    <li><a href="{{route('dashboard')}}">DASHBOARD</a></li>
                    @endcan
                    @if($page = App\Option::getOption('pageAbout'))
                        <li><a href="{{route('page',$page)}}">ABOUT</a></li>
                    @endif
                    @if($page = App\Option::getOption('pageFAQ'))
                        <li><a href="{{route('page',$page)}}">FAQ</a></li>
                    @endif
                    @if($page = App\Option::getOption('pageContacts'))
                        <li><a href="{{route('page',$page)}}">CONTACTS</a></li>
                    @endif
                    <button>USER'S MANUAL</button>
                </ul>
            </div>
        </div>
        <div id="bottom_header">
            <div class="container">
                @auth
                    <a id="open_mob_ql" href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">LOG OUT</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                @endauth
                <ul class="social">
                    <li><a href="https://www.facebook.com/TACSO3" class="fb"></a></li>
                    <li><a href="https://twitter.com/tacso1" class="tw"></a></li>
                    <li><a href="https://www.youtube.com/user/tascotube#p/u" class="yt"></a></li>
                </ul>
                <div class="divider"></div>
            </div>
        </div>
        <div id="mob_menu">
            <div class="container">
                <span class="close_menu">
                    <svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23px" height="23px">
                        <path fill-rule="evenodd"  fill="rgb(255, 255, 255)"
                         d="M16.270,11.498 L22.003,17.226 C23.324,18.543 23.324,20.682 22.003,22.002 C20.683,23.322 18.542,23.322 17.222,22.002 L11.489,16.275 L5.757,22.002 C4.436,23.322 2.296,23.322 0.975,22.002 C-0.345,20.682 -0.345,18.543 0.975,17.226 L6.708,11.498 L0.975,5.770 C-0.345,4.452 -0.345,2.311 0.975,0.993 C2.296,-0.326 4.436,-0.326 5.757,0.993 L11.489,6.721 L17.222,0.993 C18.542,-0.326 20.683,-0.326 22.003,0.993 C23.324,2.311 23.324,4.452 22.003,5.770 L16.270,11.498 Z"/>
                    </svg>
                </span>
                <ul id="menu-header-menu-1" class="mob_menu">
                    <li><a href="#">ABOUT</a></li>
                    <li><a href="#">CONTACTS</a></li>
                </ul>
               <div class="divider"></div>
            </div>
        </div>
    </header>
