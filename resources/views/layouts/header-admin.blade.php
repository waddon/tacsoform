<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/layout.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/components.min.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/colors.min.css" rel="stylesheet" type="text/css">
    <link href="/assets-admin/css/custom.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script src="/assets-admin/js/main/jquery.min.js"></script>
    <script src="/assets-admin/js/main/bootstrap.bundle.min.js"></script>
    <script src="/assets-admin/js/plugins/loaders/blockui.min.js"></script>
    <script src="/assets-admin/js/plugins/ui/ripple.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script src="/assets-admin/js/plugins/visualization/d3/d3.min.js"></script>
    <script src="/assets-admin/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script src="/assets-admin/js/plugins/forms/styling/switchery.min.js"></script>
    <script src="/assets-admin/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script src="/assets-admin/js/plugins/ui/moment/moment.min.js"></script>
    <script src="/assets-admin/js/plugins/pickers/daterangepicker.js"></script>

    <script src="/assets-admin/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="/assets-admin/js/plugins/forms/selects/select2.min.js"></script>

    <script src="/assets-admin/js/app.js"></script>
    <script src="/assets-admin/js/demo_pages/dashboard.js"></script>
    <!--script src="/assets-admin/js/demo_pages/datatables_basic.js"></script-->
    <!-- /theme JS files -->
@section('style')
@show

</head>

<body>
    <!-- Main navbar -->
    <div class="navbar navbar-expand-md navbar-dark bg-indigo navbar-static">
        <div class="navbar-brand">
            <a href="{{route('home')}}" class="d-inline-block">
                <img src="/assets/img/activemedia.png" alt="">
            </a>
        </div>

        <div class="d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
                <i class="icon-tree5"></i>
            </button>
            <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
                <i class="icon-paragraph-justify3"></i>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar-mobile">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                        <i class="icon-paragraph-justify3"></i>
                    </a>
                </li>
            </ul>

            {{--@include('menu.element-greet')--}}

            <ul class="navbar-nav ml-md-auto">
                {{--@include('menu.element-connect')--}}
                {{--@include('menu.element-activity')--}}

                <li class="nav-item">
                    <a href="#" class="navbar-nav-link" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <i class="icon-switch2"></i>
                        <span class="d-md-none ml-2">Logout</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                </li>
            </ul>
        </div>
    </div>
    <!-- /main navbar -->


    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-light sidebar-main sidebar-expand-md">

            <!-- Sidebar mobile toggler -->
            <div class="sidebar-mobile-toggler text-center">
                <a href="#" class="sidebar-mobile-main-toggle">
                    <i class="icon-arrow-left8"></i>
                </a>
                <span class="font-weight-semibold">Navigation</span>
                <a href="#" class="sidebar-mobile-expand">
                    <i class="icon-screen-full"></i>
                    <i class="icon-screen-normal"></i>
                </a>
            </div>
            <!-- /sidebar mobile toggler -->


            <!-- Sidebar content -->
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user-material">
                    <div class="sidebar-user-material-body">
                        <div class="card-body text-center">
                            <a href="#">
                                <img src="https://tacso.eu/wp-content/uploads/2019/05/logot.png" class="img-fluid mb-3" alt="">
                            </a>
                            <h6 class="mb-0 text-white text-shadow-dark">{{Auth::user()->name}}</h6>
                            {{--<span class="font-size-sm text-white text-shadow-dark">Santa Ana, CA</span>--}}
                        </div>
                                                    
                        <div class="sidebar-user-material-footer">
                            <a href="#user-nav" class="d-flex justify-content-between align-items-center text-shadow-dark dropdown-toggle" data-toggle="collapse"><span>My account</span></a>
                        </div>
                    </div>

                    <div class="collapse" id="user-nav">
                        @include('menu.profile-menu')
                    </div>
                </div>
                <!-- /user menu -->


                <!-- Main navigation -->
                <div class="card card-sidebar-mobile">
                    @include('menu.sidebar-menu')
                </div>
                <!-- /main navigation -->
                <script type="text/javascript">
                    $('.nav-link.active').parents('.nav-item-submenu').addClass('nav-item-open');
                    $('.nav-link.active').parents('.nav-group-sub').show();
                    $('.card-sidebar-mobile').show();
                </script>

            </div>
            <!-- /sidebar content -->
            
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
                <div class="page-header-content header-elements-md-inline">
                    <div class="page-title d-flex">
                        <h4>{!!$data['icon'] ?? ''!!} <span class="font-weight-semibold">{!!$data['title'] ?? ''!!}</span></h4>
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>

                    <div class="header-elements d-none">
                        {{--@include('menu.header-elements')--}}
                    </div>
                </div>

                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                    <div class="d-flex">
                        @include('menu.breadcrumb')
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>

                    <div class="header-elements d-none">
                        {{--@include('menu.breadcrumb-line-header-elements')--}}
                    </div>
                </div>
            </div>
            <!-- /page header -->

            <!-- Content area -->
            <div class="content">
