<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'status' => [
        1 => 'Filling Annex 1',
        2 => 'Waiting for Approve',
        3 => 'Annex 1 Approved',
        4 => 'Filling Annex 2',
        5 => 'Mission Complete',
    ],

    'annex1' => [
        'q1'  => '1. Name of the applicant (organisation, institution) ',
        'q2'  => '2. Type of applicant',
            'q2_1' => 'Civil Society Organisation (association, foundation, network, grass-roots organisation, community-based organization, social partner) from an IPA Beneficiary:',
                'q2_1_1' => 'Albania',
                'q2_1_2' => 'Bosnia and Herzegovina',
                'q2_1_3' => 'Kosovo*',
                'q2_1_4' => 'Montenegro',
                'q2_1_5' => 'North Macedonia',
                'q2_1_6' => 'Serbia',
                'q2_1_7' => 'Turkey',
            'q2_2' => 'EU Institution or EU Delegation in an IPA Beneficiary',
            'q2_3' => 'National Resource Centre',
            'q2_4' => 'Other (Please specify)',
                'q2_4_1' => 'Other (Please specify)',
        'q3'  => '3. Contact person ',
        'q4'  => '4. Position/role of the contact person',
        'q5'  => '5. Website and social media of the applicant (if applicable)',
        'q6'  => '6. E-mail address',
        'q7'  => '7. Phone Number',
        'q8'  => '8. Priority area of proposed event or activity (please choose from the table below the thematic area under which the proposed event falls, or specify another area under the option “other”, instead of Topic of proposed event',
            'q8-desc' => '(please choose from the table below the thematic area under which the proposed event falls, or specify another area under the option “other”',
            'q8_1' => 'CONDUCIVE ENVIRONMENT',
                'q8_1_1' => 'Fundamental rights and freedoms',
                'q8_1_2' => 'Civil society development, protection of civic space',
                'q8_1_3' => 'Good governance, transparency and accountability',
                'q8_1_4' => 'Dialogue with public authorities and influence in decision-making process',
                'q8_1_5' => 'CSOs sustainability: state funding, tax legislation, philanthropy, volunteering, economic activities',
                'q8_1_6' => 'Free access to information and open data, publicly accessible official data on CSOs',
                'q8_1_7' => 'Other (please specify):',
                    'q8_1_7_1' => 'Other (please specify):',
            'q8_2' => 'CSO CAPACITY DEVELOPMENT',
                'q8_2_1' => 'Good governance',
                'q8_2_2' => 'Increasing public trust in CSOs',
                'q8_2_3' => 'Digital literacy',
                'q8_2_4' => 'Strategic approach with MEL (Monitoring, Evaluation & Learning)',
                'q8_2_5' => 'Participation in decision making',
                'q8_2_6' => 'Diversified funding',
                'q8_2_7' => 'Human rights and gender equality',
                'q8_2_8' => 'Persistence in activism',
                'q8_2_9' => 'Other (please specify):',
                    'q8_2_9_1' => 'Other (please specify):',
        'q9'  => '9. Objective(s) of the event or activity – what are you hoping to achieve?',
        'q10'  => '10. Type of event or activity',
            'q10_1'  => 'Training',
            'q10_2'  => 'Study visit',
            'q10_3'  => 'Conferences, Networking',
            'q10_4'  => 'Workshop and forums (thematic and sectoral)',
            'q10_5'  => 'Coaching and mentoring',
            'q10_6'  => 'Internship and job shadowing',
        'q11' => '11. Proposed location',
        'q12' => '12. Proposed date',
        'q13' => '13. Proposed geographical scope',
        'q14' => '14. Main target group(s)',
        'q15' => '15. Estimated number of participants',
        'q16' => '16. Type of support requested',
            'q16_1' => 'Advice / Expertise',
            'q16_2' => 'Organisational / logistical support',
            'q16_3' => 'Financial Support',
            'q16_4' => 'Other (please specify):',
                'q16_4_1' => 'Other (please specify):',
        'q17' => '17. Please indicate if the event/activity is part of a larger project or initiative (e.g. part of EU-funded project). ',
        'q18' => '18. Please include an estimation of the total budget required for the event/activity, and the amount to be requested from the P2P Programme.',

        'p1_q1'  => '1. Name of the applicant',
        'p1_q2'  => '2. Name of the contact person',
        'p1_q3'  => '3. E-mail address of the contact person',
        'p1_q4'  => '4. Phone number of the contact person',
        'p1_q5'  => '5. Title of event/activity',
        'p1_q6'  => '6. Starting date of the event/activity',
        'p1_q7'  => '7. Duration of the event/activity',
        'p1_q8'  => '8. Location of the event/activity',
        'p1_q9'  => '9. Estimated number of participants',

        'p1_q10_1'  => 'A. Travels',
        'p1_q10_2'  => 'B. Visas',
        'p1_q10_3'  => 'C. Accommodation',
        'p1_q10_4'  => 'D. Local Transport (airport transfer, hotel-venue transfer etc.)',
        'p1_q10_5'  => 'E. Invitations',
        'p1_q10_6'  => 'F. Badges/Kits',
        'p1_q10_7'  => 'G. Prints',
        'p1_q10_8'  => 'H. Conference Room',
        'p1_q10_9'  => 'I. Conference Equipment (microphones, projector, etc.)',
        'p1_q10_10' => 'J. Catering (coffee breaks, lunch, dinner, etc.)',
        'p1_q10_11' => 'K. Speakers / Moderators ',
        'p1_q10_12' => 'L. Interpreters',
        'p1_q10_13' => 'M. Online tools',
        'p1_q10_14' => 'N. Other (please specify)',
            'p1_q10_14_3' => 'Other (please specify)',


        'p2_q1'   => '2.1. Background',
        'p2_q2'   => '2.2. Description',
        'p2_q2_1' => 'Objective of the event/activity',
        'p2_q2_2' => 'Outcomes',
        'p2_q2_3' => 'Target groups',
        'p2_q2_4' => 'Main theme/topic/area',
        'p2_q2_5' => 'Event/activity format',
        'p2_q2_6' => 'Location and geographical scope',
        'p2_q2_7' => 'Working language',
        'p2_q2_8' => 'Duration of event/activity',


        'p3_q1'  => '3.1. Budget/support',
        'p3_q2'  => '3.2. Involvement of your organisation',
        'p3_q3'  => '4. Outreach of the follow-up actions',
        'p3_q4'  => '5. Accompanying visibility activities',
        'p3_q5'  => '6. Outputs',
    ],

];
