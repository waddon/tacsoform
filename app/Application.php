<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{

    protected $guarded = [];

    protected $casts = [
        'annex1' => 'object',
        'annex2' => 'object',
        'agenda' => 'object',
        'data'   => 'object',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $model->data = (object)[];
            $model->save();
        });
    }

    public function showApplicationForm($form='f1')
    {
        $result = '';
        if (isset($this->data->{$form}) && is_object($this->data->{$form})) {
            foreach ($this->data->{$form} as $key => $value) {
                if ($value && $key!='form_id') {
                    $result .= '<tr><td>' . __('custom.annex1.'.$key) . '</td><td>' . $value . '</td></tr>';
                }
            }
        }
        return $result;
    }

}
