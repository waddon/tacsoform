<?php

namespace App\Traits;

trait BreadcrumbTrait
{
    static public function getBreadcrumb()
    {
        $controllers = [
            'UserController' => 'users',
            'RoleController' => 'roles',
            'PermissionController' => 'permissions',
            'ApplicationController' => 'applications-admin',
        ];
        $icons = [
            'UserController' => '<i class="icon-users mr-2 icon-2x"></i>',
            'RoleController' => '<i class="icon-users mr-2 icon-2x"></i>',
            'PermissionController' => '<i class="icon-users mr-2 icon-2x"></i>',
            'ApplicationController' => '<i class="icon-drawer3 mr-2 icon-2x"></i>',
        ];
        $data = [];
        $class  = debug_backtrace()[1]['class'] ? substr(strrchr(debug_backtrace()[1]['class'], "\\"), 1) : 'unknown';
        $method = debug_backtrace()[1]['function'] ? debug_backtrace()[1]['function'] : 'unknown';
        $data['breadcrumbs'] = [
            [
                'url' => route('home'),
                'title' => '<i class="icon-home2 mr-2"></i> Home',
            ],
            [
                'url' => route('dashboard'),
                'title' => 'Dashboard',
            ],
        ];
        if (isset($controllers[$class])){
            if ($method != 'index'){
                array_push($data['breadcrumbs'] , ['title' => ucwords($controllers[$class]), 'url' => route($controllers[$class].'.index')]);
                array_push($data['breadcrumbs'] , ['title' => ucwords($method . ' ' . $controllers[$class]), 'url' => '#']);
            } else {
                array_push($data['breadcrumbs'] , ['title' => ucwords($controllers[$class]), 'url' => route($controllers[$class].'.'.$method)]);
            }
        }

        $data['icon']  = isset($icons[$class]) ? $icons[$class] : '<i class="icon-home4 mr-2 icon-2x"></i>';
        $data['title'] = isset($controllers[$class]) ? ucwords($controllers[$class]) : 'Dashboard';

        if($class=='AdminController'){
            if($method=='options'){
                array_push($data['breadcrumbs'] , ['title' => __(ucwords('options')), 'url' => route('options') ]);
                $data['icon'] = '<i class="icon-cog mr-2 icon-2x"></i>';
                $data['title'] = __('Options');
            }
        }

        return $data;
    }

}