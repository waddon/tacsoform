<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'name'    => $this->name,
            'slug'    => $this->slug,
            'public'  => $this->public ? __('Public') : __('Privat'),
            'actions' => !$this->trashed() 
                ? '<div class="list-icons">'
                    . '<a href="'.route('pages.edit', $this->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>'
                    . '<a href="'.route('pages.show', $this->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>'
                    . '<a href="'.route('pages.delete', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text delete').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>'
                    . '</div>'
                : '<div class="list-icons">'
                    . '<a href="'.route('pages.restore', $this->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('Restore').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text restore').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>'
                    . '<a href="'.route('pages.destroy', $this->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('Destroy').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text destroy').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>'
                    . '</div>',
        ];
    }
}
