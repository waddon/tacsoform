<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Application;

class ApplicationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $application = Application::where('id', $request->application)->first();
            if($application && $application->user_id != Auth::user()->id) {
                return redirect()->route('home');
            }
        }

        return $next($request);
    }
}