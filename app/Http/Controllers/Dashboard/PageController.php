<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StorePageRequest;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Page;
use App\Option;

class PageController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        return view('dashboard.pages.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Page;
        return view('dashboard.pages.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePageRequest $request)
    {
        $array = [
                'name'   =>$request->name,
                'slug'   =>$request->slug,
                'content'=>$request->content ? $request->content : '',
                'public' =>$request->public ? true : false,
            ];
        $model = Page::create($array);
        return response([
            'message' => __('Element created successfully'),
            'formAction' => route('pages.update', $model->id),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Page::findOrFail($id);
        return view('dashboard.pages.show', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Page::findOrFail($id);
        return view('dashboard.pages.form', [
                'model'=>$model,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePageRequest $request, $id)
    {
        $model = Page::findOrFail($id);
        $array = [
                'name'   =>$request->name,
                'slug'   =>$request->slug,
                'content'=>$request->content ? $request->content : '',
                'public' =>$request->public ? true : false,
            ];
        $model->update($array);
        return response([
            'message' => __('Element updated successfully'),
            'formAction' => route('pages.update', $model->id),
        ], 200);
    }

    /**
     * Move to trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id)
    {
        $model = Page::withTrashed()->findOrFail($id);
        $model->delete();
        return response([], 200);
    }

    /**
     * Restore from trash the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, $id)
    {
        $model = Page::onlyTrashed()->findOrFail($id);
        $model->restore();
        return response([], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $model = Page::withTrashed()->findOrFail($id);
        $model->forceDelete();
        return response([], 200);
    }


    /**
     * Get list of data for datatable.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function datatable(Request $request)
    {
        return json_encode( Page::getListForDatatable($request) );
    }


}
