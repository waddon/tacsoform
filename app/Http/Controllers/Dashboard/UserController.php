<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;

use App\User;
use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

//Enables us to output flash messaging
use Session;
// use App\Respondent;
// use App\Respondent_user;
// use App\School;

class UserController extends Controller {

    use BreadcrumbTrait;

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('dashboard.users.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
    //Get all roles and pass it to the view
        $roles = Role::get();
        return view('dashboard.users.create', [
                'roles'=>$roles,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreUserRequest $request) {
        $user = new User;
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->password = $request->input('password');
        $user->save();

        $roles = $request['roles']; //Retrieving the roles field checking if a role was selected
        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r); //Assigning role to user
            }
        }        
    //Redirect to the users.index view and display message
        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.user'), 'name' => $user->name]));
    }



    public function show(Request $request, $id)
    {
        $newuser = User::find($id);
        if ($newuser){
            Auth::login($newuser);
        }
        return redirect()->route('home')
            ->with('flash_message', 'Вы авторизовались под пользователем ' . auth()->user()->name);
    }


    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $user = User::findOrFail($id); //Get user with specified id
        $roles = Role::get(); //Get all roles
        return view('dashboard.users.edit', [
                'user'=>$user,
                'roles'=>$roles,
                'data' => self::getBreadcrumb(),
            ]); //pass user and roles data to view

    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id); //Get role specified by id

        //Validate name, email and password fields  
        $this->validate($request, [
            'email'=>'required|email|unique:users,email,' . $id,
            'name' => 'required|string|max:255',
        ]);
        
        $input = $request->only(['email', 'name']); //Retreive the name, email and password fields
        $roles = $request['roles']; //Retreive all roles
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);  //If one or more role is selected associate user to roles          
        }        
        else {
            $user->roles()->detach(); //If no role is selected remove exisiting role associated to a user
        }
        return redirect()->route('users.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.user'), 'name' => $user->name]));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id) {
        $model = User::findOrFail($id);
        $model->delete();
        echo json_encode(true);
        die();
    }


    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
        ];

        $totalData = User::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = User::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = User::where($where)->where('name','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = User::where($where)->where('name','like',"%{$search}%");
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->name;
                $nestedData['email'] = $model->email;
                $nestedData['roles'] = $model->roles->implode('name', ', ');
                //$nestedData['status'] = trans('custom.status.'.$model->status_id);
                $nestedData['created'] = $model->created_at->format('d.m.Y');
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('users.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.show', $model->id).'" class="list-icons-item" title="'.__('Login by...').'"><i class="icon-enter6"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('users.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Remove').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

}