<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Application;

use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;

class ApplicationController extends Controller
{

    use BreadcrumbTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {        
        return view('dashboard.applications.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Application::findOrFail($id);
        return response([
            'f1' => $model->showApplicationForm('f1'),
            'f2' => $model->showApplicationForm('f2'),
            'f3' => $model->showApplicationForm('f3'),
            'f4' => $model->showApplicationForm('f4'),
            'file' => isset($model->data->file) ? $model->data->file : (object)[],
            'agenda' => is_array($model->agenda) ? $model->agenda : (object)[],
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        if ($request->table == 'approve'){
            $where = [['status_id','=',2]];
        } else if( $request->table == 'complete' ) {
            $where = [['status_id','=',5]];
        } else {
            $where = [['status_id','<>',2],['status_id','<>',5]];
        }
        $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'status_id',
            3 => 'created_at',
        ];

        $totalData = Application::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Application::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Application::where($where)
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Application::where($where);
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->user->name;
                $nestedData['status'] = trans('custom.status.'.$model->status_id);
                $nestedData['created'] = $model->created_at->format('d.m.Y');
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('applications-admin.show', $model->id).'" class="list-icons-item btn-view" title="'.__('annex 1').'" data-toggle="modal" data-target="#modal-view"><i class="icon-eye"></i></a>';
                    if($model->status_id==2){
                        $nestedData['actions'] .= '<a href="'.route('applications-admin.approve', $model->id).'" class="list-icons-item text-success-600 btn-approve" title="'.__('Approve').'" data-toggle="modal" data-target="#modal-approve"><i class="icon-check"></i></a>';
                    }
                    if($model->status_id>2){
                        $nestedData['actions'] .= '<a href="'.route('applications-admin.show', $model->id).'" class="list-icons-item btn-view2" title="'.__('annex 2').'" data-toggle="modal" data-target="#modal-view2"><i class="icon-clipboard3"></i></a>';
                    }
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }

    public function approve(Request $request, $id)
    {
        $model = Application::findOrFail($id);
        $model->update( [ 'status_id'=> 3 ] );

        $data = [
                'subject' => 'Application has been approved',
                'title' => 'Application has been approved',
                'content' => 'My content',
                'user' => $model->user->name,
            ];

        Mail::to( $model->user->email )->send(new OrderShipped($data));
        //Mail::to($request->email)
            //->cc('waddon@narod.ru')
            //->queue(new OrderShipped($data));

        return response([
            'message' => trans('cms.alert-update', ['name' => $model->created_at]),
        ], 200);
    }


    // public function show2($id)
    // {
    //     $model = Application::findOrFail($id);
    //         $result = is_object($model->annex2) ? $model->annex2 : (object)[];
    //         $result->p1_q10_1 = isset($result->p1_q10_1_1)&&$result->p1_q10_1_1 ? 'Yes' : 'No';
    //         $result->p1_q10_2 = isset($result->p1_q10_2_1)&&$result->p1_q10_2_1 ? 'Yes' : 'No';
    //         $result->p1_q10_3 = isset($result->p1_q10_3_1)&&$result->p1_q10_3_1 ? 'Yes' : 'No';
    //         $result->p1_q10_4 = isset($result->p1_q10_4_1)&&$result->p1_q10_4_1 ? 'Yes' : 'No';
    //         $result->p1_q10_5 = isset($result->p1_q10_5_1)&&$result->p1_q10_5_1 ? 'Yes' : 'No';
    //         $result->p1_q10_6 = isset($result->p1_q10_6_1)&&$result->p1_q10_6_1 ? 'Yes' : 'No';
    //         $result->p1_q10_7 = isset($result->p1_q10_7_1)&&$result->p1_q10_7_1 ? 'Yes' : 'No';
    //         $result->p1_q10_8 = isset($result->p1_q10_8_1)&&$result->p1_q10_8_1 ? 'Yes' : 'No';
    //         $result->p1_q10_9 = isset($result->p1_q10_9_1)&&$result->p1_q10_9_1 ? 'Yes' : 'No';
    //         $result->p1_q10_10 = isset($result->p1_q10_10_1)&&$result->p1_q10_10_1 ? 'Yes' : 'No';
    //         $result->p1_q10_11 = isset($result->p1_q10_11_1)&&$result->p1_q10_11_1 ? 'Yes' : 'No';
    //         $result->p1_q10_12 = isset($result->p1_q10_12_1)&&$result->p1_q10_12_1 ? 'Yes' : 'No';
    //         $agenda = is_array($model->agenda) ? $model->agenda : (object)[];
    //     return response([
    //         'message' => $result,'agenda' => $agenda,
    //     ], 200);
    // }
}
