<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Traits\BreadcrumbTrait;

use App\User;
use App\Application;
use App\Option;
use App\Page;
use Auth;
use App\Http\Controllers\Controller;

use Session;

class AdminController extends Controller 
{ 

    use BreadcrumbTrait;

    public function index()
    {
        $stat = [
            'users' => User::count(),
            'wait' => Application::where('status_id','=','2')->count(),
            'complete' => Application::where('status_id','=','5')->count(),
        ];
        return view('dashboard.index', [
            'data' => self::getBreadcrumb(),
            'stat' => $stat,
        ]);
    }

    public function options(Request $request)
    {

        if ($request->isMethod('post')) {
            foreach ($request->all() as $key => $value) {
                Option::setOption($key, $value ? $value : '');
            }
            return redirect()->route('options')
                ->with('flash_message', __('Options updated successfully'));
        }        
        return view('dashboard.options',[
                'data' => self::getBreadcrumb(),
                'options' => Option::pluck('value','key'),
                'infopagesAnnex1' => Page::getList( Option::getOption('infopagesAnnex1') ),
                'infopagesAnnex2' => Page::getList( Option::getOption('infopagesAnnex2') ),
                'pages' => Page::all(),
            ]);
    }

}