<?php

namespace App\Http\Controllers\Dashboard;

use App\Traits\BreadcrumbTrait;

use Illuminate\Http\Request;

use Auth;
use App\Http\Controllers\Controller;

//Importing laravel-permission models
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Session;

class PermissionController extends Controller {

    use BreadcrumbTrait;

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request) {
        return view('dashboard.permissions.index', [
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create() {
        $roles = Role::get(); //Get all roles
        return view('dashboard.permissions.create', [
            'roles'=>$roles,
            'data' => self::getBreadcrumb(),
        ]);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request) {
        $this->validate($request, [
            'name'=>'required|max:255',
        ]);

        $name = $request['name'];
        $permission = new Permission();
        $permission->name = $name;

        $roles = $request['roles'];

        $permission->save();



        if (!empty($request['roles'])) { //If one or more role is selected
            foreach ($roles as $role) {
                $r = Role::where('id', '=', $role)->firstOrFail(); //Match input role to db record

                $permission = Permission::where('name', '=', $name)->first(); //Match input //permission to db record
                $r->givePermissionTo($permission);
            }
        }

        return redirect()->route('permissions.index')
            ->with('flash_message', trans('messages.appended', ['item'=>trans('main.permission'), 'name' => $permission->name]));

    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id) {
        return redirect('permissions');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id) {
        $permission = Permission::findOrFail($id);
        return view('dashboard.permissions.edit', [
                'permission'=>$permission,
                'data' => self::getBreadcrumb(),
            ]);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:255',
        ]);
        $input = $request->all();
        $permission->fill($input)->save();

        return redirect()->route('permissions.index')
            ->with('flash_message', trans('messages.edited', ['item'=>trans('main.permission'), 'name' => $permission->name]));
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request, $id) {
        $model = Permission::findOrFail($id);
        if ($model->name != "admin"){
            $model->delete();
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
        die();
    }

    # функция для получения аяксом данных для datatable
    public function datatable(Request $request)
    {
        $where = [];
        $columns = [
            0 => 'id',
        ];

        $totalData = Permission::where($where)->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $temp = Permission::where($where)->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $totalFiltered = $totalData;
        } else {
            $search = $request->input('search.value');
            $temp = Permission::where($where)->where('name','like',"%{$search}%")
                ->offset($start)
                ->orderby($order,$dir)
                ->limit($limit);
                $models = $temp->get();
            $temp = Permission::where($where)->where('name','like',"%{$search}%");
                $totalFiltered = $temp->count();
        }
        
        $data = [];
        if($models){
            foreach($models as $model){
                $nestedData['id'] = $model->id;
                $nestedData['name'] = $model->name;
                $nestedData['roles'] = $model->roles->implode('name', ', ');;
                $nestedData['actions'] = '<div class="list-icons">';
                    $nestedData['actions'] .= '<a href="'.route('permissions.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
                    $nestedData['actions'] .= '<a href="'.route('permissions.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-destroy" title="'.__('Remove').'" data-toggle="modal" data-target="#modal-ajax-destroy"><i class="icon-trash"></i></a>';
                $nestedData['actions'] .= '</div>';
                $data[] = $nestedData;
            }
        }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => $data,

        ];
        echo json_encode($json_data);
    }
}