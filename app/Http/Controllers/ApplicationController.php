<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Application;
use App\Page;
use App\Option;
use Storage;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        # Если нет applications - создаем
        if(Auth::user()->applications()->count()==0){
            // Application::create([
            //     'user_id'=>Auth::user()->id,
            // ]);
            return redirect()->route('applications.create');
        }
        if(Auth::user()->applications()->count()==1 && Auth::user()->applications()->first()->status_id<3){
            return redirect()->route('applications.edit',Auth::user()->applications()->first()->id);
        } else {
            return view('applications.index', []);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Application::create([
            'user_id'=>Auth::user()->id,
        ]);
        return redirect()->route('applications.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Application::findOrFail($id);
        return view('applications.show', [
            'model'=>$model,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Application::findOrFail($id);
        if ($model->status_id == 1) {
            return view('applications.annex1',  
                [
                    'model'=>$model,
                    'infopages'=>Page::getList( Option::getOption('infopagesAnnex1') )
                ]
            );
        } elseif ($model->status_id == 2) {
            return view('applications.message', ['message'=>'PLEASE WAIT, YOUR PROFILE IS PENDING']);
        } elseif (in_array($model->status_id, [3, 4])) {
            return view('applications.annex2',  
                [
                    'model'=>$model,
                    'infopages'=>Page::getList( Option::getOption('infopagesAnnex2') )
                ]
            );
        } else {
            return view('applications.message', ['message'=>'YOUR APPLICATION FORM WAS SUCCESSFULLY SENT']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $model = Application::findOrFail($id);
        // if($request->annex1){
        //     $model->update( [ 'annex1'=> str_replace("'", "’", $request->annex1) ] );
        // } elseif($request->annex2){
        //     $model->update( [ 
        //         'annex2'=> str_replace("'", "’", $request->annex2),
        //         'agenda'=> str_replace("'", "’", $request->agenda),
        //         'status_id'=>4,
        //     ] );
        // }
        // return response([
        //     'message' => trans('cms.alert-update', ['name' => $model->created_at]),
        // ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function approve(Request $request, $id)
    {
        $model = Application::findOrFail($id);
        $data = $model->data ? $model->data : (object)[];
        $form_id = isset($request->form_id) ? $request->form_id : 1;
        $status = isset($request->status) ? $request->status : (($form_id=='f1') ? 2 : 4);

        foreach ($request->file() as $file) {
            # удаляем старый файл, если есть
            if (isset($data->file->url) && Storage::disk('public')->exists($data->file->url)){
                Storage::disk('public')->delete($data->file->url);
            }
            $mimetype = $file->getMimeType();
            $type = explode( '/', $mimetype )[0];
            $path = Storage::disk('public')->putFile($type, $file);
            $data->file = (object)[
                'name'=>$file->getClientOriginalName(),
                'size'=>$file->getSize(),
                'type'=>$type,
                'mimetype'=>$mimetype,
                'url'=>$path,
            ];
        }

        $data->{$form_id} = $request->input();
        $model->update( [
            'status_id' => $status,
            'data' => $data,
        ] );
        return response([
            'message' => trans('Form ' . $form_id . ' saved'),
        ], 200);
    }

    public function postupdate(Request $request, $id)
    {
        $model = Application::findOrFail($id);
        if($request->annex1){
            $model->update( [ 'annex1'=> str_replace("'", "’", $request->annex1) ] );
        } elseif($request->annex2){
            $model->update( [ 
                'annex2'=> str_replace("'", "’", $request->annex2),
                'agenda'=> str_replace("'", "’", $request->agenda),
                'status_id'=>4,
            ] );
        }
        return response([
            'message' => trans('cms.alert-update', ['name' => $model->created_at]),
        ], 200);
    }
}
