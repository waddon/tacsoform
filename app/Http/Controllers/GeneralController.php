<?php

namespace App\Http\Controllers;

use App\User;
use App\Page;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{

    public function index(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route('applications.index');
        } else {
            return view('form');
        }
    }

    public function postLogin(LoginRequest $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials, $request->remember_me)) {
            return response()->json(['redirect'=>'/']);
        } else {
            return response()->json(['error'=>'Wrong credentials'], 500);
        }
    }

    public function postRegister(StoreUserRequest $request)
    {
        $credentials = $request->only('name', 'email');
        $credentials['password'] = bcrypt($request->password);
        if ($user = User::create($credentials)) {
            Auth::login($user);
            return response()->json(['redirect'=>'/']);
        } else {
            return response()->json(['error'=>'Wrong credentials'], 500);
        }

    }

    public function page(Request $request, $id)
    {
        return view('page',['model'=>Page::findOrFail($id)]);
    }

}
