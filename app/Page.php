<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Resources\PageResource;
use Illuminate\Http\Request;

class Page extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public static $columns = [
            0 => 'id',
            1 => 'name',
            2 => 'slug',
            3 => 'public',
    	];

    static public function getListForDatatable(Request $request)
    {
        $search = $request->input('search.value');
        $totalData = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->count();

        $totalFiltered = self::when( $request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->when( !empty( $search ), function($query) use ($search) {
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->count();
        $models = self::when( !empty( $search ), function($query) use ($search){
                    return $query->where(
                        [
                            ['id', '=', (int)$search, 'or'],
                            ['name', 'like', "%{$search}%", 'or']
                        ]);
                })
            ->when($request->table==='trash', function($query){
                    return $query->onlyTrashed();
                })
            ->offset($request->input('start'))
            ->orderby(self::$columns[$request->input('order.0.column')], $request->input('order.0.dir'))
            ->limit($request->input('length'))
            ->get();

        $data = ( PageResource::collection($models) );
        // if($models){
        //     foreach($models as $model){
        //         $nestedData['id'] = $model->id;
        //         $nestedData['name'] = $model->name;
        //         $nestedData['slug'] = $model->slug;
        //         $nestedData['public'] = $model->public;
        //         $nestedData['actions'] = '<div class="list-icons">';
        //         if ($request->table!='trash'){
        //             $nestedData['actions'] .= '<a href="'.route('pages.edit', $model->id).'" class="list-icons-item" title="'.__('Edit').'"><i class="icon-pencil7"></i></a>';
        //             $nestedData['actions'] .= '<a href="'.route('pages.show', $model->id).'" class="list-icons-item" title="'.__('Preview').'"><i class="icon-eye"></i></a>';
        //             $nestedData['actions'] .= '<a href="'.route('pages.delete', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('Delete').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text delete').'" data-color="bg-danger" data-method="POST"><i class="icon-trash"></i></a>';
        //         } else {
        //             $nestedData['actions'] .= '<a href="'.route('pages.restore', $model->id).'" class="list-icons-item text-success-600 btn-ajax-action" title="'.__('Restore').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text restore').'" data-color="bg-success" data-method="POST"><i class="icon-undo"></i></a>';
        //             $nestedData['actions'] .= '<a href="'.route('pages.destroy', $model->id).'" class="list-icons-item text-danger-600 btn-ajax-action" title="'.__('Destroy').'" data-toggle="modal" data-target="#modal-ajax-action" data-text="'.__('Text destroy').'" data-color="bg-danger" data-method="DELETE"><i class="icon-x"></i></a>';
        //         }

        //         $nestedData['actions'] .= '</div>';
        //         $data[] = $nestedData;
        //     }
        // }
        $json_data = [
            "draw" => (int)$request->input('draw'),
            "recordsTotal" => (int)$totalData,
            "recordsFiltered" => (int)$totalFiltered,
            "data" => PageResource::collection($models),

        ];

        return $json_data;
    }

    static public function getList($elements = [])
    {
        $result = [];
        if (!is_array($elements)) {$elements = json_decode($elements);}
        if (is_array($elements)){
            foreach ($elements as $key => $element) {
                if ($model = self::find($element))
                    $result[] = $model;
            }
        }
        return $result;
    }
}